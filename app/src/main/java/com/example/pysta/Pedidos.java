package com.example.pysta;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.media.MediaScannerConnection;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Looper;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Filterable;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.pysta.dataAccess.Endpoints;
import com.example.pysta.dataAccess.RetrofitClientInstance;
import com.example.pysta.dataAccess.SqliteRoutines;
import com.example.pysta.models.Clientes;
import com.example.pysta.models.InformacionPedidos;
import com.example.pysta.models.Vendedor;
import com.github.gcacace.signaturepad.views.SignaturePad;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.textfield.TextInputLayout;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TreeMap;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;
import static android.view.View.GONE;
import static com.google.android.gms.location.LocationServices.getFusedLocationProviderClient;

/**
 * @Description: Esta Clase me permite crear los pedidos por vendeor.
 * * @Autor: Carolina Ñañez Valencia.
 * * @FechaCreacion: 7-5-2021.
 **/
public class Pedidos extends AppCompatActivity {
	private EditText CantidadPrducto, edtDescripccion,valorEditablePedido;
	private TextView ValorproductoTotal, configProducto, listaProductos, nameProducto, textValorTotal,
			Valorproducto,editTextFirma,txtDescripccion;
	private CardView cardsPedidos;
	private ImageView imgDelete, imgCheck;
	private RecyclerView mRecyclerView;
	private RecyclerView.LayoutManager recyclerViewLayoutManager;
	private Context context;
	private JSONArray pedidosArray = new JSONArray();
	private MuaAdapter adapterPedidos;
	private RadioGroup opciones;
	private RadioGroup radioGroupTipoLugar;
	private LinearLayout linearcheckbox,linearPrincipal;
	private Button btnEnviarPedido;
	private JSONArray clientesArray = new JSONArray();
	private JSONArray arrayPedidoFinalizado = new JSONArray();
	private AutoCompleteTextView actv,actvsProducto;
	private List<String> listaClientes = new ArrayList<>();
	private List<String> listaCategorias = new ArrayList<>();
	private List<String> listProductos = new ArrayList<>();
	private JSONArray jsonArrayClientes, getJsonArrayProductos;
	private int idClientePedido = 0;
	private JSONObject ArrayPedidos;
	private Map<String, Pedido> jugadores = new TreeMap<String, Pedido>();
	private String nombreProductoSelecionado, tipoPago = "null", observacion,path,grupo,categoria;
	private int  clienteProspecto, zonaCliente,idVendedor,cantidadProducto, zonaVender;
	private Clientes informacionClientes;
	private ProgressBar simpleProgressBar;
	private SqliteRoutines sqliteRoutines;
	private Vendedor vendedor;
	private InformacionPedidos informacionPedidos;
	String valorPedidoLimpio, nombreProductoList;
	private Aplicacion app;
	private Clientes  cliente;
	private LocationManager ubicacion;
	private String ubicacion2;
	private Double latitud, longitud;
	private ArrayList GeolocalizacionLongitud,GeolocalizacionLatitud;
	private JSONObject dataUbicacion;
	private int valorProductos, cantidadProductos = 0,idproducto;
	private TextInputLayout textInputvalorEditablePedido;
	private ImageButton btnBack;
	private boolean flagLocaliacionCliente, flagInformacionGeolocalizacionCliente,certificaCliente,
			flagPicture= false;
	private SignaturePad signature_pad;
	private  File f,wallpaperDirectory;
	private Double valorProduct, sumaTotal = 0.0;
	private LocationRequest mLocationRequest;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_pedidos);
		app = (Aplicacion) getApplicationContext();
		//reciclerView
		mRecyclerView = findViewById(R.id.recyclerViewPedidos);
		recyclerViewLayoutManager = new GridLayoutManager(context, 1);
		mRecyclerView.setLayoutManager(recyclerViewLayoutManager);
		mRecyclerView.setHasFixedSize(true);
		sqliteRoutines = new SqliteRoutines(this);
		vendedor = sqliteRoutines.readVendedor();
		idVendedor = vendedor.getIdUsuario();
		zonaVender = Integer.valueOf(vendedor.getIdZonaVendedor());
		GeolocalizacionLongitud = new ArrayList();
		GeolocalizacionLatitud = new ArrayList();
		obtenerClientes();
		obtenerProductos();
		localizacion();
		mayRequestStoragePermission();



		//textView y editext
		cardsPedidos = findViewById(R.id.cardsPedidos);
		imgDelete = findViewById(R.id.imgDelete);
		imgCheck = findViewById(R.id.imgCheck);
		configProducto = findViewById(R.id.configProducto);
		listaProductos = findViewById(R.id.listaProductos);
		linearcheckbox = findViewById(R.id.linearcheckbox);
		linearPrincipal = findViewById(R.id.linearPrincipal);
		nameProducto = findViewById(R.id.nameProducto);
		Valorproducto = findViewById(R.id.Valorproducto);
		valorEditablePedido = findViewById(R.id.valorEditablePedido);
		textInputvalorEditablePedido = findViewById(R.id.textInputvalorEditablePedido);
		txtDescripccion = findViewById(R.id.txtDescripccion);
		edtDescripccion = findViewById(R.id.edtDescripccion);
		btnEnviarPedido = findViewById(R.id.btnEnviarPedido);
		textValorTotal = findViewById(R.id.textValorTotal);
		btnBack = findViewById(R.id.btnBack);
		simpleProgressBar = findViewById(R.id.progressBar);
		simpleProgressBar.setProgress ( 90 );
		linearPrincipal.setVisibility(GONE);

		//Creating the instance of ArrayAdapter containing list of fruit names

		//Getting the instance of AutoCompleteTextView
		actvsProducto = (AutoCompleteTextView) findViewById(R.id.autoCompleteDescripccion);
		actv = (AutoCompleteTextView) findViewById(R.id.autoCompleteTextView);
		actv.setThreshold(1);//will start working from first character

		actv.setTextColor(getResources().getColor(R.color.colorsubText));

		actv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				String clienteSeleccionado = String.valueOf(parent.getItemAtPosition(position));
				Log.v("Palabra", String.valueOf(parent.getItemAtPosition(position)));
				String latitudCliente = null;
				String longitudCliente = null;
				for (int i = 0; i < jsonArrayClientes.length(); i++) {
					JSONObject jsonObjectClientes = null;
					try {
						jsonObjectClientes = jsonArrayClientes.getJSONObject(i);

						String BuscarNombreCliente = jsonObjectClientes.getString("sucursal");
						if (clienteSeleccionado.equals(BuscarNombreCliente)) {
							idClientePedido = jsonObjectClientes.getInt("idCliente");
							clienteProspecto = jsonObjectClientes.getInt("prospecto");
							zonaCliente = jsonObjectClientes.getInt("idZonaCliente");

							String ubicaciones = jsonObjectClientes.getString("ubicacionCliente");
							if (ubicaciones != "null") {

								JSONObject ubicacionesCliente = jsonObjectClientes.getJSONObject("ubicacionCliente");

								latitudCliente = ubicacionesCliente.getString("latitud");
								longitudCliente = ubicacionesCliente.getString("longitud");
							}else {
								latitudCliente = "0.0";
								longitudCliente = "0.0";
							}
							
							informacionClientes = new Clientes(
									jsonObjectClientes.getString("nombreCliente"),
									jsonObjectClientes.getString("sucursal"),
									jsonObjectClientes.getString("nitOcc"),
									jsonObjectClientes.getString("correoCliente"),
									jsonObjectClientes.getString("direccionCliente"),
									jsonObjectClientes.getString("telefonoCliente"),
									jsonObjectClientes.getInt("idZonaCliente"),
									jsonObjectClientes.getInt("prospecto"),
									Double.valueOf(latitudCliente),
									Double.valueOf(longitudCliente),
									jsonObjectClientes.getInt("idCliente")
							);
							if (Double.valueOf(latitudCliente) != 0){
								if(latitud == null){
									LayoutInflater factory = LayoutInflater.from(Pedidos.this);
									final View deleteDialogView = factory.inflate(R.layout.alert, null);
									final AlertDialog deleteDialog = new AlertDialog.Builder(Pedidos.this).create();
									deleteDialog.setView(deleteDialogView);
									final TextView Title = (TextView) deleteDialogView.findViewById(R.id.txtTitle);
									Title.setText(R.string.important);
									final TextView Descripcion = (TextView) deleteDialogView.findViewById(R.id.txtDescribe);
									Descripcion.setText("No ha activado la función de ubicación en su dispositivo o verifique que haya aceptado el permiso para poder obtener la ubicación.");
									idClientePedido = 0;
									actv.setText("");
									TextView dialogBtn_cancel = deleteDialogView.findViewById(R.id.btn_cancel);
									dialogBtn_cancel.setOnClickListener(new View.OnClickListener() {
										@Override
										public void onClick(View v) {
											deleteDialog.dismiss();

										}
									});

									TextView dialogBtn_okay = (TextView) deleteDialogView.findViewById(R.id.btn_ok);
									dialogBtn_okay.setOnClickListener(new View.OnClickListener() {
										@Override
										public void onClick(View v) {
											localizacion();
											deleteDialog.dismiss();
										}
									});

									deleteDialog.show();
								}else {
									flagLocaliacionCliente = localizacionCliente();
								}

							}else {
								flagInformacionGeolocalizacionCliente = true;
							}

						}
					} catch (JSONException e) {
						e.printStackTrace();
					}
				}
			}
		});

		//get the spinner from the xml.
		Spinner dropdown = findViewById(R.id.spinner1);
		//create a list of items for the spinner.
		String[] items = new String[]{"Seleccione una opcion","GR1 DIAMANTE", "GR1 PLATINO", "GR1 MAYORISTA"};
		listaCategorias.add("Seleccione una Categoria");
		//Grupo 1
		listaCategorias.add("GR1 Diamante");
		listaCategorias.add("GR1 Platimum");
		listaCategorias.add("GR1 Mayorista");
		// Grupo 2
		listaCategorias.add("GR2 Diamante");
		listaCategorias.add("GR2 Platimum");
		listaCategorias.add("GR2 Mayorista");

		//Grupo 3
		listaCategorias.add("GR3 Diamante");
		listaCategorias.add("GR3 Platimum");
		listaCategorias.add("GR3 Mayorista");


		//create an adapter to describe how the items are displayed, adapters are used in several places in android.
		//There are multiple variations of this, but this is the basic variant.
		ArrayAdapter<String> adapter = new ArrayAdapter<>(this,
				android.R.layout.simple_spinner_dropdown_item, listaCategorias);
		//set the spinners adapter to the previously created one.
		dropdown.setAdapter(adapter);

		dropdown.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
									   int position, long id) {
				Log.v("item", (String) parent.getItemAtPosition(position));
				Log.v("posicion", String.valueOf(position));
				pedidosArray = new JSONArray();
				adapterPedidos = new MuaAdapter(Pedidos.this, pedidosArray);
				mRecyclerView.setAdapter(adapterPedidos);
				if (position != 0){
					txtDescripccion.setVisibility(View.VISIBLE);
					cardsPedidos.setVisibility(View.GONE);
					actvsProducto.setVisibility(View.VISIBLE);
					actvsProducto.setText("");
					configProducto.setVisibility(View.GONE);
					pedidosArray = new JSONArray();
					adapterPedidos = new MuaAdapter(Pedidos.this, pedidosArray);
					mRecyclerView.setAdapter(adapterPedidos);
					listaProductos.setVisibility(View.GONE);
					linearcheckbox.setVisibility(View.GONE);
					mRecyclerView.setVisibility(View.GONE);
					edtDescripccion.setVisibility(View.GONE);
					textValorTotal.setVisibility(View.GONE);
					textValorTotal.setText("0");
					jugadores.clear();

					if (pedidosArray.length() > 0) {
						pedidosArray = new JSONArray();
						adapterPedidos = new MuaAdapter(Pedidos.this, pedidosArray);
						mRecyclerView.setAdapter(adapterPedidos);
						listaProductos.setVisibility(View.GONE);
						linearcheckbox.setVisibility(View.GONE);
						mRecyclerView.setVisibility(View.GONE);
						cardsPedidos.setVisibility(View.GONE);
						edtDescripccion.setVisibility(View.GONE);
						textValorTotal.setVisibility(View.GONE);
						textValorTotal.setText("0");
						jugadores.clear();
					}

					String categoriaSeleccionada = (String) parent.getItemAtPosition(position);
					String[] Partes = categoriaSeleccionada.split(" ");
					grupo = Partes[0]; //Grupo
					categoria = Partes[1]; // Categoria

				}else {
					txtDescripccion.setVisibility(GONE);
					actvsProducto.setVisibility(GONE);
					pedidosArray = new JSONArray();
					adapterPedidos = new MuaAdapter(Pedidos.this, pedidosArray);
					mRecyclerView.setAdapter(adapterPedidos);
					listaProductos.setVisibility(View.GONE);
					linearcheckbox.setVisibility(View.GONE);
					mRecyclerView.setVisibility(View.GONE);
					edtDescripccion.setVisibility(View.GONE);
					textValorTotal.setVisibility(View.GONE);
					textValorTotal.setText("0");
				}
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {
				// TODO Auto-generated method stub
			}
		});


		actvsProducto.setThreshold(1);//will start working from first character
		actvsProducto.setTextColor(getResources().getColor(R.color.colorsubText));

		actvsProducto.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				nombreProductoSelecionado = String.valueOf(parent.getItemAtPosition(position));

				for (int i = 0; i < getJsonArrayProductos.length(); i++) {
					try {
						JSONObject jsonObjectProducto = getJsonArrayProductos.getJSONObject(i);

						String nombreProducto = jsonObjectProducto.getString("descripcion");
						if (nombreProductoSelecionado.equals(nombreProducto)) {
							idproducto= 0;
							idproducto = jsonObjectProducto.getInt("idproducto");
							//valorProductos = jsonObjectProducto.getInt("precio");
							String precioProducto = jsonObjectProducto.getString("precioJson");

							if (precioProducto != "null"){
								JSONObject jsonObjectCategorias= getJsonArrayProductos.getJSONObject(i).getJSONObject("precioJson");
								JSONObject jsonObjectGrupo= jsonObjectCategorias.getJSONObject(grupo);
								String valorCategoria = jsonObjectGrupo.getString(categoria);
								valorProduct = Double.parseDouble(valorCategoria);
								Log.v("PrecioCAtegoria", valorCategoria);
							}else {
								valorProduct = 0.0;
							}
						}

					} catch (JSONException e) {
						e.printStackTrace();
					}
				}

				cardsPedidos.setVisibility(View.VISIBLE);
				nameProducto.setText(String.valueOf(parent.getItemAtPosition(position)));
				Valorproducto.setText("$" + Tools.formatNumberDouble(valorProduct));

				if(valorProduct > 0.0){
					Valorproducto.setVisibility(View.VISIBLE);
					valorEditablePedido.setVisibility(View.GONE);
					textInputvalorEditablePedido.setVisibility(View.GONE);
				}else {
					Valorproducto.setVisibility(View.GONE);
					valorEditablePedido.setVisibility(View.VISIBLE);
					textInputvalorEditablePedido.setVisibility(View.VISIBLE);
				}

				configProducto.setVisibility(View.VISIBLE);

			}
		});

		btnBack.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(Pedidos.this, Home.class);
				startActivity(intent);
				finish();
			}
		});

		btnEnviarPedido.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				observacion = edtDescripccion.getText().toString();
				if (idClientePedido == 0) {
					Toast.makeText(getApplicationContext(),
							R.string.selecCliente, Toast.LENGTH_SHORT).show();
				} else if (jugadores.isEmpty()) {
					Toast.makeText(getApplicationContext(),
							R.string.emptyProducto, Toast.LENGTH_SHORT).show();

				} else if (tipoPago.equals("null")) {
					Toast.makeText(getApplicationContext(),
							R.string.emptyModoPago, Toast.LENGTH_SHORT).show();
				} else if(dataUbicacion.length() == 0){
					LayoutInflater factory = LayoutInflater.from(Pedidos.this);
					final View deleteDialogView = factory.inflate(R.layout.alert, null);
					final AlertDialog deleteDialog = new AlertDialog.Builder(Pedidos.this).create();
					deleteDialog.setView(deleteDialogView);
					final TextView Title = (TextView) deleteDialogView.findViewById(R.id.txtTitle);
					Title.setText(R.string.important);
					final TextView Descripcion = (TextView) deleteDialogView.findViewById(R.id.txtDescribe);
					Descripcion.setText("No ha activado la función de ubicación en su dispositivo o verifique que haya aceptado el permiso para poder obtener la ubicación.");

					TextView dialogBtn_cancel = deleteDialogView.findViewById(R.id.btn_cancel);
					dialogBtn_cancel.setOnClickListener(new View.OnClickListener() {
						@Override
						public void onClick(View v) {
							deleteDialog.dismiss();

						}
					});

					TextView dialogBtn_okay = (TextView) deleteDialogView.findViewById(R.id.btn_ok);
					dialogBtn_okay.setOnClickListener(new View.OnClickListener() {
						@Override
						public void onClick(View v) {
							startLocationUpdates();
							deleteDialog.dismiss();
						}
					});

					deleteDialog.show();
				}else{

					for (Map.Entry<String, Pedido> jugador : jugadores.entrySet()) {
						String clave = jugador.getKey();
						Pedido valor = jugador.getValue();
						System.out.println(clave + "  ->  " + valor.valorTotal);
						cantidadProducto = valor.cantidad;
						int productoId = valor.idProducto;
						valorPedidoLimpio = valor.valorUnitario;

						nombreProductoList = valor.nommbrePedido;

						ArrayPedidos = new JSONObject();
						try {
							ArrayPedidos.put("cantidad", cantidadProducto);
							ArrayPedidos.put("idProducto", productoId);
							ArrayPedidos.put("descripcion", nombreProductoList);
							ArrayPedidos.put("valor unidad", valorPedidoLimpio);

						} catch (JSONException e) {
							e.printStackTrace();
						}
						arrayPedidoFinalizado.put(ArrayPedidos);
					}

					if (flagInformacionGeolocalizacionCliente){
						LayoutInflater factory = LayoutInflater.from(Pedidos.this);
						final View deleteDialogView = factory.inflate(R.layout.alertactualizacion, null);
						final AlertDialog deleteDialog = new AlertDialog.Builder(Pedidos.this).create();
						deleteDialog.setView(deleteDialogView);
						final TextView Title = (TextView) deleteDialogView.findViewById(R.id.txtTitle);
						Title.setText(R.string.important);
						final TextView Descripcion = (TextView) deleteDialogView.findViewById(R.id.txtDescribe);
						Descripcion.setText(R.string.geolocaliazcionClienenull);

						TextView dialogBtn_cancel = deleteDialogView.findViewById(R.id.btn_NotActualizar);
						dialogBtn_cancel.setOnClickListener(new View.OnClickListener() {
							@Override
							public void onClick(View v) {
								deleteDialog.dismiss();
								certificaCliente = false;
								enviarInformacionPedido();
							}
						});

						TextView dialogBtn_okay = (TextView) deleteDialogView.findViewById(R.id.btn_Actualizar);
						dialogBtn_okay.setOnClickListener(new View.OnClickListener() {
							@Override
							public void onClick(View v) {

								deleteDialog.dismiss();
								certificaCliente = true;
								enviarInformacionPedido();
							}
						});

						deleteDialog.show();
					}else {
						if(flagLocaliacionCliente){
							certificaCliente = false;
							enviarInformacionPedido();
						}else {
							LayoutInflater factory = LayoutInflater.from(Pedidos.this);
							final View deleteDialogView = factory.inflate(R.layout.alert, null);
							final AlertDialog deleteDialog = new AlertDialog.Builder(Pedidos.this).create();
							deleteDialog.setView(deleteDialogView);
							final TextView Title = (TextView) deleteDialogView.findViewById(R.id.txtTitle);
							Title.setText(R.string.important);
							final TextView Descripcion = (TextView) deleteDialogView.findViewById(R.id.txtDescribe);
							Descripcion.setText(R.string.geolocaliazcionClieneFail);

							TextView dialogBtn_cancel = deleteDialogView.findViewById(R.id.btn_cancel);
							dialogBtn_cancel.setOnClickListener(new View.OnClickListener() {
								@Override
								public void onClick(View v) {
									deleteDialog.dismiss();

								}
							});

							TextView dialogBtn_okay = (TextView) deleteDialogView.findViewById(R.id.btn_ok);
							dialogBtn_okay.setOnClickListener(new View.OnClickListener() {
								@Override
								public void onClick(View v) {

									deleteDialog.dismiss();
									enviarInformacionPedido();
									certificaCliente = false;
								}
							});

							deleteDialog.show();
						}
						Log.v("llego", "Falso");
					}

				}
			}
		});

		CantidadPrducto = findViewById(R.id.CantidadPrducto);
		ValorproductoTotal = findViewById(R.id.ValorproductoTotal);

		CantidadPrducto.addTextChangedListener(new TextWatcher() {

			// Antes de que el texto cambie (no debemos modificar nada aquí)
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {

			}

			//Cuando esté cambiando...(no debemos modificar el texto aquí)
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {

			}

			@Override
			public void afterTextChanged(Editable s) {
				String elNuevoTexto = s.toString();
				// Hacer lo que sea con elNuevoTexto

				if (!elNuevoTexto.isEmpty()) {
					cantidadProductos = Integer.valueOf(elNuevoTexto);
					Double valor = Integer.valueOf(cantidadProductos) * valorProduct;
					ValorproductoTotal.setText("$" + Tools.formatNumberDouble(valor));
				}
			}
		});

		//Si el valor viene null aparaece el campo para agregar el valor
		valorEditablePedido.addTextChangedListener(new TextWatcher() {

			// Antes de que el texto cambie (no debemos modificar nada aquí)
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {

			}

			//Cuando esté cambiando...(no debemos modificar el texto aquí)
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {

			}

			@Override
			public void afterTextChanged(Editable s) {
				String elNuevoTexto = s.toString();
				// Hacer lo que sea con elNuevoTexto

				if (!elNuevoTexto.isEmpty()) {
					Double valor = Double.valueOf(elNuevoTexto) ;
					valorProduct = valor;



					Double valores = Integer.valueOf(cantidadProductos) * valorProduct;
					ValorproductoTotal.setText("$" + Tools.formatNumberDouble(valores));
				}else {
					valorProductos  = 0;
					ValorproductoTotal.setText("$" + Tools.formatNumberDouble(0.0));
				}
			}
		});

		imgDelete.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				cardsPedidos.setVisibility(GONE);
				CantidadPrducto.setText("0");
				ValorproductoTotal.setText("$" + Tools.formatNumberDouble(0.0));
				configProducto.setVisibility(GONE);
				actvsProducto.setText("");
			}
		});

		imgCheck.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {

				if (CantidadPrducto.getText().toString().equals("")) {
					Toast.makeText(getApplicationContext(),
							"Debe ingresar la cantidad de unidades del pedido antes de continuar",
							Toast.LENGTH_SHORT).show();



				} else  {
					if (valorProduct == 0){
						Toast.makeText(getApplicationContext(),
								"Debe ingresar el valor del producto para continuar",
								Toast.LENGTH_SHORT).show();

					}else {
						cardsPedidos.setVisibility(GONE);
						actvsProducto.setText("");
						int cantidadProducto = Integer.parseInt(CantidadPrducto.getText().toString());
						String valorTotal = ValorproductoTotal.getText().toString();
						if (cantidadProducto != 0) {
							JSONObject jsonObject = new JSONObject();
							String nombreProducto = nombreProductoSelecionado;
							String valorUnitario = "$" + Tools.formatNumberDouble(valorProduct);
							try {
								jsonObject.put("cantidad", cantidadProducto);
								jsonObject.put("idProducto", idproducto);
								jsonObject.put("nombreProducto", nombreProducto);
								jsonObject.put("valorUnitario", valorUnitario);
								jsonObject.put("valorTotal", valorTotal);

								ArrayPedidos = new JSONObject();
								try {
									ArrayPedidos.put("cantidad", cantidadProducto);
									ArrayPedidos.put("idProducto", idproducto);
									ArrayPedidos.put("descripcion", nombreProducto);
									ArrayPedidos.put("valor unidad", valorUnitario);

								} catch (JSONException e) {
									e.printStackTrace();
								}

								if (jugadores.isEmpty()) {
									jugadores.put(nombreProducto, new Pedido(cantidadProducto,idproducto,
											nombreProducto, valorUnitario, valorTotal));
									pedidosArray.put(jsonObject);
									adapterPedidos = new MuaAdapter(Pedidos.this, pedidosArray);
									mRecyclerView.setAdapter(adapterPedidos);
								} else {
									if (jugadores.containsKey(nombreProducto)) {
										Toast.makeText(getApplicationContext(),
												"El producto " + nombreProducto +
														" ya está ingresado en la lista", Toast.LENGTH_SHORT).show();
									} else {
										jugadores.put(nombreProducto, new Pedido(cantidadProducto,idproducto,
												nombreProducto, valorUnitario, valorTotal));
										pedidosArray.put(jsonObject);
										adapterPedidos = new MuaAdapter(Pedidos.this, pedidosArray);

										mRecyclerView.setAdapter(adapterPedidos);
									}
								}
								sumaTotal = 0.0;
								actualizarValor();
								valorProductos = 0;
								cantidadProductos = 0;
								ValorproductoTotal.equals(0);
								valorEditablePedido.setText("");

							} catch (JSONException e) {
								e.printStackTrace();
							}
						}

						CantidadPrducto.setText("");
						ValorproductoTotal.setText("$" + Tools.formatNumberDouble(0.0));
						radioGroupTipoLugar.setVisibility(View.VISIBLE);
						configProducto.setVisibility(GONE);
						listaProductos.setVisibility(View.VISIBLE);
						linearcheckbox.setVisibility(View.VISIBLE);
						mRecyclerView.setVisibility(View.VISIBLE);
						edtDescripccion.setVisibility(View.VISIBLE);
						textValorTotal.setVisibility(View.VISIBLE);

						InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
						imm.hideSoftInputFromWindow(imgCheck.getWindowToken(), 0);
					}
				}

			}
		});

		//RadioGroup
		radioGroupTipoLugar = findViewById(R.id.radioGroupTipoLugar);
		final RadioButton radioButtonContado = findViewById(R.id.rbcontado);
		final RadioButton radioButtonCredito = findViewById(R.id.rbcredito);

		radioGroupTipoLugar.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(RadioGroup radioGroup, int i) {
				int idDeRadioButtonSeleccionado = radioGroupTipoLugar.getCheckedRadioButtonId();
				if (idDeRadioButtonSeleccionado == radioButtonContado.getId()) {
					tipoPago = "Contado";
				} else if (idDeRadioButtonSeleccionado == radioButtonCredito.getId()) {
					tipoPago = "Credito";
				} else {
					// Mmmh, imposible pero pon el else por las dudas
				}
			}
		});
	}

	//Funcion que permte optener la distancia entre el cliente y la posicion del vendedor.
	private boolean localizacionCliente() {
	boolean	flaGeolocalizacionCliente = true;
	double distMax = 100.0;
		Location cliente = new Location("localizacionCliente");
		cliente.setLatitude(informacionClientes.getLatitud());
		cliente.setLongitude(informacionClientes.getLongitud());


		Location localizacionGPS =new Location("localizacionGPS");
		localizacionGPS.setLatitude(latitud);
		localizacionGPS.setLongitude(longitud);

		float distance = cliente.distanceTo(localizacionGPS);

		if( distance <= distMax ){
			System.out.println("La distancia es menor");
			flaGeolocalizacionCliente = true;
		}else {
			System.out.println("La distancia es mayor");
			flaGeolocalizacionCliente = false;
		}

		return flaGeolocalizacionCliente;
	}

	//Funcion que trae todos los productos disponibles.
	private void obtenerProductos() {
		Endpoints endpoints = RetrofitClientInstance.getRetrofitInstance().create(Endpoints.class);

		Call<String> call = endpoints.productos();
		call.enqueue(new Callback<String>() {
			@Override
			public void onResponse(Call<String> call, Response<String> response) {
				try {

					getJsonArrayProductos = new JSONArray(response.body());

					Log.v("Productos", String.valueOf(getJsonArrayProductos));
					for (int i = 0; i < getJsonArrayProductos.length(); i++) {
						JSONObject jsonObjectProducto = getJsonArrayProductos.getJSONObject(i);

						String nombreProducto = jsonObjectProducto.getString("descripcion");
						listProductos.add(nombreProducto);

						ArrayAdapter<String> adapters = new ArrayAdapter<String>(Pedidos.this,
								R.layout.producto, R.id.autoCompleteItem, listProductos);
						actvsProducto.setAdapter(adapters);
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}

			@Override
			public void onFailure(Call<String> call, Throwable t) {
				Toast.makeText(Pedidos.this,
						R.string.internet,
						Toast.LENGTH_SHORT).show();
			}
		});
	}

	//Obtiene el permiso para poder guardar la informacion de la firma.
	private boolean mayRequestStoragePermission() {
		 final int MY_PERMISSIONS = 100;
		if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M)
			return true;
		if ((checkSelfPermission(WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) &&
				(checkSelfPermission(READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED)) {
			return true;
		} else {
			requestPermissions(new String[]{WRITE_EXTERNAL_STORAGE, READ_EXTERNAL_STORAGE}, MY_PERMISSIONS);
		}
		return false;
	}

	//Este metodo Permite abrir el alert de confirmacion del pedido.
	private void enviarInformacionPedido(){
		LayoutInflater factoryPedido = LayoutInflater.from(Pedidos.this);
		final View deleteDialogViewPedido = factoryPedido.inflate(R.layout.alert_pedidos, null);
		final AlertDialog deleteDialog = new AlertDialog.Builder(Pedidos.this).create();
		deleteDialog.setView(deleteDialogViewPedido);
		final TextView nombreCliente =  deleteDialogViewPedido.findViewById(R.id.txtCliente);
		final TextView direccionCliente =  deleteDialogViewPedido.findViewById(R.id.textDireccion);
		final TextView emailCLiente =  deleteDialogViewPedido.findViewById(R.id.textEmail);
		final TextView nombreVendedor =  deleteDialogViewPedido.findViewById(R.id.txtVendedor);
		final TextView fecha =  deleteDialogViewPedido.findViewById(R.id.textFecha);
		final TextView iva =  deleteDialogViewPedido.findViewById(R.id.iva);
		final RecyclerView recyclerView =  deleteDialogViewPedido.findViewById(R.id.recyclerView);
		final LinearLayout linearObservacion = deleteDialogViewPedido.findViewById(R.id.linearObservacion);
		final TextView textObservacion =  deleteDialogViewPedido.findViewById(R.id.textObservacion);

		 editTextFirma =  deleteDialogViewPedido.findViewById(R.id.editTextFirma);
		 signature_pad = deleteDialogViewPedido.findViewById(R.id.signature_pad);

		MuaAdapterPedido adapterPedidos;
		RecyclerView.LayoutManager recyclerViewLayoutManagers;
		recyclerViewLayoutManagers = new GridLayoutManager(context, 1);
		recyclerView.setLayoutManager(recyclerViewLayoutManagers);
		recyclerView.setHasFixedSize(true);
		adapterPedidos = new MuaAdapterPedido(Pedidos.this, pedidosArray);
		recyclerView.setAdapter(adapterPedidos);
		final TextView tipoPedido =  deleteDialogViewPedido.findViewById(R.id.tipoPedido);
		final TextView totalPedido =  deleteDialogViewPedido.findViewById(R.id.totalPedido);


		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.getDefault());
		Date date = new Date();

		String fechaActual = dateFormat.format(date);

		nombreCliente.setText(informacionClientes.getSucursal());
		direccionCliente.setText(informacionClientes.getDireccionCliente());
		emailCLiente.setText(informacionClientes.getCorreoCliente());
		nombreVendedor.setText(vendedor.getNombreVendedor());
		fecha.setText(fechaActual);
		String valor = String.valueOf(Tools.formatNumberDouble(sumaTotal));
		totalPedido.setText(" $" + valor);
		tipoPedido.setText(tipoPago);
		iva.setText("Incluye iva");

		if (observacion.equals("")){
			linearObservacion.setVisibility(View.GONE);
		}else {

			linearObservacion.setVisibility(View.VISIBLE);
			textObservacion.setText(observacion);
		}



		TextView dialogBtn_cancel = deleteDialogViewPedido.findViewById(R.id.btn_cancelar);
		dialogBtn_cancel.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				deleteDialog.dismiss();

			}
		});

		TextView dialogBtn_okay = (TextView) deleteDialogViewPedido.findViewById(R.id.btn_confirm);
		dialogBtn_okay.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {

				if (flagPicture == true){
					String pathSD = Environment.getExternalStorageDirectory().getAbsolutePath() + "/GlobalCircuit/";   // ruta

					File drctrio = new File(pathSD);
					if (!drctrio.exists()) {
						drctrio.mkdirs();
					}

					Bitmap bitmap = signature_pad.getSignatureBitmap();
					try {
						path = saveImage(bitmap);
					} catch (IOException e) {
						e.printStackTrace();
					}
				}

				enviarInformacionPedidoConfirmado();
			//	deleteDialog.dismiss();

			}
		});



		deleteDialog.show();
	}

	public void onCheckboxClicked(View view) {
		// Is the view now checked?
		boolean checked = ((CheckBox) view).isChecked();

		// Check which checkbox was clicked
		switch(view.getId()) {
			case R.id.checkbox_firma:
				if (checked){
					editTextFirma.setVisibility(View.VISIBLE);
					signature_pad.setVisibility(View.VISIBLE);
					flagPicture = true;
				}else{
					editTextFirma.setVisibility(View.GONE);
					signature_pad.setVisibility(View.GONE);
					signature_pad.clearView();
					flagPicture = false;
				}
				// Put some meat on the sandwich

				// Remove the meat
				break;

		}
	}


	//Funcion que permite Guardar la foto en el dispositivo
	private String saveImage(Bitmap myBitmap) throws IOException {

		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.getDefault());
		Date date = new Date();

		String fechaHoraFoto = dateFormat.format(date);

		ByteArrayOutputStream bytes = new ByteArrayOutputStream();
		myBitmap.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
		wallpaperDirectory = new File(
				Environment.getExternalStorageDirectory() + "/GlobalCircuit/" );
		if (!wallpaperDirectory.exists()) {
			wallpaperDirectory.mkdirs();
			Log.d("hhhhh",wallpaperDirectory.toString());
		}

		try {
			f = new File(wallpaperDirectory, informacionClientes.getIdCliente()+"-"+fechaHoraFoto+ ".jpg");
			f.createNewFile();


			Log.v("response", f.getName());
			FileOutputStream fo = new FileOutputStream(f);
			fo.write(bytes.toByteArray());
			MediaScannerConnection.scanFile(Pedidos.this,
					new String[]{f.getPath()},
					new String[]{"image/jpeg"}, null);
			fo.close();

			return f.getAbsolutePath();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		return "";


	}


	//Funcion que permite enviar el pedido realizado y actualzar la informacion del cliente si es necesario.
	private void enviarInformacionPedidoConfirmado() {
		String urlFirma;
		if (flagPicture){
			urlFirma = String.valueOf(f);
			enviarFirma();
		}else {
			urlFirma = "null";
		}

			//Certifica si solo actualiza la informacion del pospecto del cliente o su geolocalizacion tambien
		Endpoints endpoints = RetrofitClientInstance.getRetrofitInstance().create(Endpoints.class);
		JSONObject dataPedido = new JSONObject();
		try {
			dataPedido.put("idClientePedido", idClientePedido);
			dataPedido.put("idVendedorPedido", idVendedor);
			dataPedido.put("descripcion", arrayPedidoFinalizado);
			dataPedido.put("totalPedido", sumaTotal);
			dataPedido.put("observacionesPedido", observacion);
			dataPedido.put("modoPago", tipoPago);
			dataPedido.put("ubicacion", dataUbicacion);
			dataPedido.put("firma", urlFirma);

		} catch (JSONException e) {
			e.printStackTrace();
		}
		Call<String> call = endpoints.nuevoPedido(dataPedido.toString());
		call.enqueue(new Callback<String>() {
			@Override
			public void onResponse(Call<String> call, Response<String> response) {
				try {
					Log.v("response", String.valueOf(response));
					JSONObject jsonObject = new JSONObject(response.body());
					String message = jsonObject.getString("message");
					int prospecto;
					Toast.makeText(getApplicationContext(),
							message, Toast.LENGTH_SHORT).show();

					if (!certificaCliente){
						if (clienteProspecto == 1) {
							prospecto = 0;
							JSONObject dataUbicacionCliente = new JSONObject();
							try {
								dataUbicacionCliente.put("latitud", informacionClientes.getLatitud());
								dataUbicacionCliente.put("longitud", informacionClientes.getLongitud());


							} catch (JSONException e) {
								e.printStackTrace();
							}

							cambiarClienteProspecto(prospecto, dataUbicacionCliente);
						}else {
							Intent intent = new Intent(Pedidos.this, Home.class);
							startActivity(intent);
							finish();
						}
					}else {
						if (clienteProspecto == 1) {
							prospecto = 0;
							if(informacionClientes.getLatitud()==0.0){
								cambiarClienteProspecto(prospecto, dataUbicacion);
							}else {

								JSONObject dataUbicacionCliente = new JSONObject();
								try {
									dataUbicacionCliente.put("latitud", informacionClientes.getLatitud());
									dataUbicacionCliente.put("longitud", informacionClientes.getLongitud());


								} catch (JSONException e) {
									e.printStackTrace();
								}

								cambiarClienteProspecto(prospecto, dataUbicacionCliente);
							}

						}else {

							if(informacionClientes.getLatitud()==0.0){
								cambiarClienteProspecto(informacionClientes.getProspecto(), dataUbicacion);
							}else {

								Intent intent = new Intent(Pedidos.this, Home.class);
								startActivity(intent);
								finish();

							}


						}
					}

				} catch (Exception e) {
					e.printStackTrace();
				}
			}

			@Override
			public void onFailure(Call<String> call, Throwable t) {


				Toast.makeText(Pedidos.this,
						R.string.internet,
						Toast.LENGTH_SHORT).show();


			}
		});






	}

	//Funcion que permite  enviar la firma del vendedor.
	private void enviarFirma() {
		if (f.exists()) {
			Log.v("exitse", "exitse");
		}else {
			if (!f.exists()) {
				Log.v("exitse", "exitse");
			}
		}

		MultipartBody.Part part = null;
		RequestBody fileReqBody = RequestBody.create(MediaType.parse("image/jpeg"), f);
		part = MultipartBody.Part.createFormData("image", f.getName(), fileReqBody);

		Endpoints endpoints = RetrofitClientInstance.getRetrofitInstance().create(Endpoints.class);

		Call<ResponseBody> call ;
		try {
			call = endpoints.enviarFirma(part);
			call.enqueue(new Callback<ResponseBody>() {
				@Override
				public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
					Log.v("response", String.valueOf(response));
				}

				@Override
				public void onFailure(Call<ResponseBody> call, Throwable t) {

					Toast.makeText(getApplicationContext(),
							R.string.internet,
							Toast.LENGTH_SHORT).show();
				}
			});
		} catch (Exception e) {
			e.printStackTrace();
		}
	}



	//Funcion que permite  actualzar la informacion del cliente si es necesario.
	private void cambiarClienteProspecto(int prospecto, JSONObject dataUbicacion) {

			app.clienteProspecto(informacionClientes.getNombreCliente(), informacionClientes.getSucursal(),informacionClientes.getNitOcc(),
					informacionClientes.getCorreoCliente(),informacionClientes.getDireccionCliente(),
					informacionClientes.getTelefonoCliente(),zonaCliente,prospecto,
					informacionClientes.getIdCliente(), dataUbicacion);
			Intent intent = new Intent(Pedidos.this, Home.class);
			startActivity(intent);

	}

	//Funcion que permite actualzar el valor en porcentaje de pesos.
	private void actualizarValor() {
		for (Map.Entry<String, Pedido> jugador : jugadores.entrySet()) {
			String clave = jugador.getKey();
			Pedido valor = jugador.getValue();
			System.out.println(clave + "  ->  " + valor.valorTotal);
			String valorProducto = valor.valorTotal;
			String valorPedidoLimpio = valorProducto.replace("$", "").replace(",", "");
			sumaTotal += Double.parseDouble(valorPedidoLimpio);
			textValorTotal.setText("Valor total Pedido $" + Tools.formatNumberDouble(sumaTotal));
		}
	}

	//Funcion que trae todos los clientes disponibles por zona del vendedor.
	private void obtenerClientes() {
		Endpoints endpoints = RetrofitClientInstance.getRetrofitInstance().create(Endpoints.class);
		JSONObject dataZonaCliente = new JSONObject();
		try {
			dataZonaCliente.put("idZonaCliente", zonaVender);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		Call<String> call = endpoints.getByZona(dataZonaCliente.toString());
		call.enqueue(new Callback<String>() {
			@Override
			public void onResponse(Call<String> call, Response<String> response) {
				try {

					jsonArrayClientes = new JSONArray(response.body());
					for (int i = 0; i < jsonArrayClientes.length(); i++) {
						JSONObject jsonObjectClientes = jsonArrayClientes.getJSONObject(i);
						Double clienteLatitud;
						Double clienteLongitud;
						String ubicaciones = jsonObjectClientes.getString("ubicacionCliente");
						if (ubicaciones != "null"){
							JSONObject ubicacionesCliente = jsonObjectClientes.getJSONObject("ubicacionCliente");

							String latitudCliente = ubicacionesCliente.getString("latitud");
							String longitudCliente = ubicacionesCliente.getString("longitud");


							if (latitudCliente.equals("undefined")){
								clienteLatitud = 0.0;
								clienteLongitud = 0.0;
							}else {
								clienteLatitud = Double.valueOf(latitudCliente);
								clienteLongitud = Double.valueOf(longitudCliente);
							}

						}else {
							clienteLatitud = 0.0;
							clienteLongitud = 0.0;
						}



						Clientes clientes = new Clientes(
								jsonObjectClientes.getString("nombreCliente"),
								jsonObjectClientes.getString("sucursal"),
								jsonObjectClientes.getString("nitOcc"),
								jsonObjectClientes.getString("correoCliente"),
								jsonObjectClientes.getString("direccionCliente"),
								jsonObjectClientes.getString("telefonoCliente"),
								jsonObjectClientes.getInt("idZonaCliente"),
								jsonObjectClientes.getInt("prospecto"),
								clienteLatitud,
								clienteLongitud,
								jsonObjectClientes.getInt("idCliente")
						);

						String nombreCliente = jsonObjectClientes.getString("sucursal");

						clientesArray.put(clientes);
						Log.v("Cliente", String.valueOf(clientesArray));
						listaClientes.add(nombreCliente);

						ArrayAdapter<String> adapter = new ArrayAdapter<String>(Pedidos.this,
								R.layout.producto, R.id.autoCompleteItem, listaClientes);


						actv.setAdapter(adapter);
					}
					simpleProgressBar.setVisibility(GONE);
					linearPrincipal.setVisibility(View.VISIBLE);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}

			@Override
			public void onFailure(Call<String> call, Throwable t) {
				Toast.makeText(Pedidos.this,
						R.string.internet,
						Toast.LENGTH_SHORT).show();
			}
		});
	}


	/**
	 * @Description: Adapter que me permite llenar la informacion de los pedidos en el reciclerView.
	 **/
	public class MuaAdapter extends RecyclerView.Adapter<MuaAdapter.ViewHolder> {
		private JSONArray tagsList;
		private JSONArray ArrayListUnits;
		private Context context;

		public MuaAdapter(Context context, JSONArray modelList) {
			this.tagsList = modelList;
			this.ArrayListUnits = tagsList;
			this.context = context;
		}


		public class ViewHolder extends RecyclerView.ViewHolder {
			public TextView textView;
			public TextView cantidad, nombrePedido, valorUnitario, ValorTotal;
			public TextView capitaletter, origen;
			public ImageView eliminarProducto;
			public LinearLayout linearscroll, layout_pay;

			public ViewHolder(View v) {
				super(v);
				linearscroll = v.findViewById(R.id.linearscroll);
				cantidad = itemView.findViewById(R.id.textCantidad);
				nombrePedido = itemView.findViewById(R.id.txtnombrePedido);
				valorUnitario = itemView.findViewById(R.id.txtValorPedido);
				ValorTotal = itemView.findViewById(R.id.txtValorTotal);
				eliminarProducto = itemView.findViewById(R.id.eliminarProducto);
				//     capitaletter = itemView.findViewById(R.id.textCapitaletter);
			}
		}

		@Override
		public MuaAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
			View view1 = LayoutInflater.from(context).inflate(R.layout.recycler_view, parent, false);
			MuaAdapter.ViewHolder viewHolder1 = new MuaAdapter.ViewHolder(view1);
			return viewHolder1;
		}


		@Override
		public void onBindViewHolder(MuaAdapter.ViewHolder Vholder, final int position) {
			try {
				final JSONObject tag = tagsList.getJSONObject(position);

				Vholder.cantidad.setText(tag.getString("cantidad"));
				Vholder.nombrePedido.setText(tag.getString("nombreProducto"));
				Vholder.valorUnitario.setText(tag.getString("valorUnitario"));
				Vholder.ValorTotal.setText(tag.getString("valorTotal"));
				String nombre = tag.getString("nombreProducto");
				char fistName = nombre.charAt(0);

				Vholder.linearscroll.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						for (int i = 0; i < tagsList.length(); i++) {
							if (i == position) {
								try {
									JSONObject ClienteSelect = tagsList.getJSONObject(position);

								} catch (Exception e) {
									e.printStackTrace();
								}
							}
						}
					}
				});

				Vholder.eliminarProducto.setOnClickListener(new View.OnClickListener() {
					@RequiresApi(api = Build.VERSION_CODES.KITKAT)
					@Override
					public void onClick(View v) {
						try {
							JSONObject produtoEliminado = tagsList.getJSONObject(position);
							String nombrePedidosEliminar = produtoEliminado.getString("nombreProducto");
							if (jugadores.containsKey(nombrePedidosEliminar)) {
								jugadores.remove(nombrePedidosEliminar);
								tagsList.remove(position);
								notifyDataSetChanged();
								sumaTotal = 0.0;
								actualizarValor();
							}

							if (jugadores.isEmpty()) {
								cardsPedidos.setVisibility(GONE);
								configProducto.setVisibility(GONE);
								linearcheckbox.setVisibility(GONE);
								mRecyclerView.setVisibility(GONE);
								listaProductos.setVisibility(View.GONE);
								edtDescripccion.setVisibility(GONE);
								edtDescripccion.setText("");
								textValorTotal.setVisibility(GONE);

							}

						} catch (JSONException e) {
							e.printStackTrace();
						}
					}
				});
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}


		@Override
		public int getItemCount() {
			return tagsList.length();
		}


	}

	public class MuaAdapterPedido extends RecyclerView.Adapter<MuaAdapterPedido.ViewHolder>  {
		private JSONArray tagsList;
		private JSONArray ArrayListUnits;
		private Context context;

		public MuaAdapterPedido(Context context, JSONArray modelList) {
			this.tagsList = modelList;
			this.ArrayListUnits = tagsList;
			this.context = context;
		}


		public class ViewHolder extends RecyclerView.ViewHolder {
			public TextView textView;
			public TextView cantidad, nombrePedido, valorUnitario, ValorTotal;
			public TextView capitaletter, origen;
			public ImageView eliminarProducto;
			public LinearLayout linearscroll, layout_pay;

			public ViewHolder(View v) {
				super(v);
				linearscroll = v.findViewById(R.id.linearscroll);
				cantidad = itemView.findViewById(R.id.textCantidad);
				nombrePedido = itemView.findViewById(R.id.txtnombrePedido);
				valorUnitario = itemView.findViewById(R.id.txtValorPedido);
				ValorTotal = itemView.findViewById(R.id.txtValorTotal);
				//     capitaletter = itemView.findViewById(R.id.textCapitaletter);
			}
		}

		@Override
		public MuaAdapterPedido.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
			View view1 = LayoutInflater.from(context).inflate(R.layout.recycler_view_vistaprevia_pedido, parent, false);
			MuaAdapterPedido.ViewHolder viewHolder1 = new MuaAdapterPedido.ViewHolder(view1);
			return viewHolder1;
		}


		@Override
		public void onBindViewHolder(MuaAdapterPedido.ViewHolder Vholder, final int position) {
			try {
				final JSONObject tag = tagsList.getJSONObject(position);

				Vholder.cantidad.setText(tag.getString("cantidad"));
				Vholder.nombrePedido.setText(tag.getString("nombreProducto"));
				Vholder.valorUnitario.setText(tag.getString("valorUnitario"));
				Vholder.ValorTotal.setText(tag.getString("valorTotal"));
				String nombre = tag.getString("nombreProducto");
				char fistName = nombre.charAt(0);

				Vholder.linearscroll.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						for (int i = 0; i < tagsList.length(); i++) {
							if (i == position) {
								try {
									JSONObject ClienteSelect = tagsList.getJSONObject(position);

								} catch (Exception e) {
									e.printStackTrace();
								}
							}
						}
					}
				});


			} catch (JSONException e) {
				e.printStackTrace();
			}
		}


		@Override
		public int getItemCount() {
			return tagsList.length();
		}


	}


	public void localizacion() {


		ubicacion = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

		if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
			ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION,}, 1000);

		}



		Location loc = ubicacion.getLastKnownLocation(LocationManager.GPS_PROVIDER);

		if(loc!=null){
			latitud = loc.getLatitude();
			longitud= loc.getLongitude();

			dataUbicacion = new JSONObject();
			try {
				dataUbicacion.put("latitud", latitud);
				dataUbicacion.put("longitud", longitud);


			} catch (JSONException e) {
				e.printStackTrace();
			}

		}else {
			startLocationUpdates();
		}



	}


	// Trigger new location updates at interval
	protected void startLocationUpdates() {

		// Create the location request to start receiving updates
		mLocationRequest = new LocationRequest();
		mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);


		// Create LocationSettingsRequest object using location request
		LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
		builder.addLocationRequest(mLocationRequest);
		LocationSettingsRequest locationSettingsRequest = builder.build();

		// Check whether location settings are satisfied
		// https://developers.google.com/android/reference/com/google/android/gms/location/SettingsClient
		SettingsClient settingsClient = LocationServices.getSettingsClient(this);
		settingsClient.checkLocationSettings(locationSettingsRequest);

		// new Google API SDK v11 uses getFusedLocationProviderClient(this)
		getFusedLocationProviderClient(this).requestLocationUpdates(mLocationRequest, new LocationCallback() {
					@Override
					public void onLocationResult(LocationResult locationResult) {
						// do work here
						onLocationChanged(locationResult.getLastLocation());
					}
				},
				Looper.myLooper());
	}

	public void onLocationChanged(Location location) {
		// New location has now been determined
		String msg = "Updated Location: " +
				Double.toString(location.getLatitude()) + "," +
				Double.toString(location.getLongitude());

		latitud = location.getLatitude();
		longitud= location.getLongitude();

		dataUbicacion = new JSONObject();
		try {
			dataUbicacion.put("latitud", latitud);
			dataUbicacion.put("longitud", longitud);


		} catch (JSONException e) {
			e.printStackTrace();
		}


	}
}


