package com.example.pysta;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Looper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.example.pysta.dataAccess.Endpoints;
import com.example.pysta.dataAccess.RetrofitClientInstance;
import com.example.pysta.dataAccess.Session;
import com.example.pysta.dataAccess.SqliteRoutines;
import com.example.pysta.models.Clientes;
import com.example.pysta.models.InformacionPedidos;
import com.example.pysta.models.Jornada;
import com.example.pysta.models.Vendedor;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.SettingsClient;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.google.android.gms.location.LocationServices.getFusedLocationProviderClient;

public class Home extends AppCompatActivity {

    private CardView cardPedidos, cardResgistroClientes,cardIniciarJornada,cardFinalizarJornada,cardVisita;
    private SqliteRoutines sqliteRoutines;
    private Vendedor vendedor;
    private TextView user_name,zonaUser;
    private Clientes clientes;
    private LocationManager ubicacion;
    private ImageButton btnlogout;
    private InformacionPedidos informacionPedidos;
    private Aplicacion app;
    private JSONArray arrayPedidoFinalizado = new JSONArray();
    public ArrayList GeolocalizacionLongitud,GeolocalizacionLatitud;
    private JSONObject ArrayPedidos;
    private int idVendedor,idjornadaGuardada,idFinalizacionJornada;
    private Session session;
    private String fechaHoraInicioJornada,idUsuarioJornada,fechaJOrnadaGuadarda,latitudes, longitudes;
    private boolean flag = true;
    public ArrayList allLocalizacionLatitud,allLocalizacionLongitud;
    private  JSONObject dataUbicacionPedido;
    private Double  latitud, longitud;
    private LocationRequest mLocationRequest;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        app = (Aplicacion) getApplicationContext();
        session = new Session(getApplicationContext());
        cardPedidos = findViewById(R.id.cardPedidos);
        cardResgistroClientes = findViewById(R.id.cardResgistroClientes);
        sqliteRoutines = new SqliteRoutines(this);
        user_name = findViewById(R.id.user_name);
        zonaUser = findViewById(R.id.zonaUser);
        cardIniciarJornada = findViewById(R.id.cardIniciarJornada);
        cardFinalizarJornada = findViewById(R.id.cardFinalizarJornada);
        cardVisita = findViewById(R.id.cardVisita);
        btnlogout = findViewById(R.id.btnlogout);
        //validacionDePEdidosPendientes();
        //validarCLientesProspectos();
        vendedor = sqliteRoutines.readVendedor();
        zonaUser.setText("Zona: "+ vendedor.getNombreZona());
        user_name.setText(vendedor.getNombreVendedor() + " "+ vendedor.getApellidoVendedor());
        idVendedor = vendedor.getIdUsuario();

        allLocalizacionLatitud = new ArrayList();
        allLocalizacionLongitud = new ArrayList();

        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.ACCESS_COARSE_LOCATION}, /* Este codigo es para identificar tu request */ 1);

        if (ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
                        != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION,
            }, 1000);
        }


        cardResgistroClientes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Home.this, RegistroClientes.class);
                startActivity(intent);
            }
        });

        cardPedidos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Home.this, Pedidos.class);
                startActivity(intent);
            }
        });


        cardVisita.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Home.this, Visitas.class);
                startActivity(intent);
            }
        });



        cardIniciarJornada.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (session.getUserDetails().get("flag").equals("true")) {
                    // btnJornada.setEnabled(true);
                    //btnJornadaFinal.setEnabled(false);
                    LayoutInflater factory = LayoutInflater.from(Home.this);
                    final View deleteDialogView = factory.inflate(R.layout.alert, null);
                    final AlertDialog deleteDialog = new AlertDialog.Builder(Home.this).create();
                    deleteDialog.setView(deleteDialogView);
                    final TextView Title = (TextView) deleteDialogView.findViewById(R.id.txtTitle);
                    Title.setText(R.string.important);
                    final TextView Descripcion = (TextView) deleteDialogView.findViewById(R.id.txtDescribe);
                    Descripcion.setText("Ya cuenta con una Jornada Iniciada");

                    TextView dialogBtn_cancel = deleteDialogView.findViewById(R.id.btn_cancel);
                    dialogBtn_cancel.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            deleteDialog.dismiss();

                        }
                    });

                    TextView dialogBtn_okay = (TextView) deleteDialogView.findViewById(R.id.btn_ok);
                    dialogBtn_okay.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            deleteDialog.dismiss();
                        }
                    });

                    deleteDialog.show();
                    
                }else {
                    String localizacionFlag = "false";
                    session.localizacionFlag(localizacionFlag);
                    SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm", Locale.getDefault());
                    Date date = new Date();

                    String horainicioJornada = dateFormat.format(date);
                    LayoutInflater factory = LayoutInflater.from(Home.this);
                    final View deleteDialogView = factory.inflate(R.layout.alert, null);
                    final AlertDialog deleteDialog = new AlertDialog.Builder(Home.this).create();
                    deleteDialog.setView(deleteDialogView);
                    final TextView Title = (TextView) deleteDialogView.findViewById(R.id.txtTitle);
                    Title.setText(R.string.important);
                    final TextView Descripcion = (TextView) deleteDialogView.findViewById(R.id.txtDescribe);
                    Descripcion.setText("Iniciara jornada a las : " + horainicioJornada + "\r\n"+
                            "Esta aplicación recoge datos de ubicación para habilitar las funciones en segundo plano aunque la aplicación esté cerrada o no se esté usando.");


                    TextView dialogBtn_cancel = deleteDialogView.findViewById(R.id.btn_cancel);
                    dialogBtn_cancel.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            deleteDialog.dismiss();

                        }
                    });

                    TextView dialogBtn_okay = (TextView) deleteDialogView.findViewById(R.id.btn_ok);
                    dialogBtn_okay.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            GeolocalizacionLongitud = new ArrayList();
                            GeolocalizacionLatitud = new ArrayList();
                            localizacion();
                            deleteDialog.dismiss();

                        }
                    });

                    deleteDialog.show();
                }



            }
        });

        btnlogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
             boolean vendedorOff =   sqliteRoutines.deleteVendedor(vendedor.getIdUsuario());
             if (vendedorOff){
                 Intent intent = new Intent(Home.this, MainActivity.class);
                 startActivity(intent);
                 finish();
             }
            }
        });


        cardFinalizarJornada.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                    if (sqliteRoutines.readFinalizarJornada() != null) {

                        List<Jornada> datosSqlidFinalizacionJornada = sqliteRoutines.readFinalizarJornada();
                        Log.v("customer lista", String.valueOf(datosSqlidFinalizacionJornada));
                        JSONObject ubicacion = new JSONObject();

                        for (int i = 0; i < datosSqlidFinalizacionJornada.size(); i++) {

                            int sincronizados = datosSqlidFinalizacionJornada.get(i).getJornadaSincronizada();

                            if (sincronizados == 1) {
                                idUsuarioJornada = datosSqlidFinalizacionJornada.get(i).getIdUsuarioJornada();
                                fechaJOrnadaGuadarda = datosSqlidFinalizacionJornada.get(i).getFecha();
                                idjornadaGuardada = datosSqlidFinalizacionJornada.get(i).getIdjornada();
                                JSONObject ubicacionJOrnada = datosSqlidFinalizacionJornada.get(i).getUbicacion();
                                idFinalizacionJornada = datosSqlidFinalizacionJornada.get(i).getId();


                                try {
                                    latitudes = ubicacionJOrnada.getString("latitud");
                                    longitudes = ubicacionJOrnada.getString("longitud");
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                if (!latitudes.equals(null)) {
                                    String nuevaLatiud = latitudes.replace("[", "")
                                            .replace("]", "").replace(",", "");
                                    String nuevaLongitud = longitudes.replace("[", "")
                                            .replace("]", "").replace(",", "");


                                    Double latitud = Double.valueOf(nuevaLatiud);
                                    Double longitud = Double.valueOf(nuevaLongitud);


                                    allLocalizacionLatitud.add(latitud);
                                    allLocalizacionLongitud.add(longitud);

                                    sqliteRoutines.deleteSincronizacionJornada(idFinalizacionJornada);
                                } else {
                                    sqliteRoutines.deleteSincronizacionJornada(idFinalizacionJornada);
                                }


                            }
                        }


                        JSONObject dataUbicaciones = new JSONObject();
                        try {
                            dataUbicaciones.put("latitud", allLocalizacionLatitud);
                            dataUbicaciones.put("longitud", allLocalizacionLongitud);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        Log.v("Datos", String.valueOf(dataUbicaciones));
                        irSincronizarJornadaFinal(idUsuarioJornada, fechaJOrnadaGuadarda, idjornadaGuardada,
                                dataUbicaciones, idFinalizacionJornada);
                        flag = false;
                        session.flag(String.valueOf(flag));

                    } else {
                        LayoutInflater factory = LayoutInflater.from(Home.this);
                        final View deleteDialogView = factory.inflate(R.layout.alert, null);
                        final AlertDialog deleteDialog = new AlertDialog.Builder(Home.this).create();
                        deleteDialog.setView(deleteDialogView);
                        final TextView Title = (TextView) deleteDialogView.findViewById(R.id.txtTitle);
                        Title.setText(R.string.important);
                        final TextView Descripcion = (TextView) deleteDialogView.findViewById(R.id.txtDescribe);
                        Descripcion.setText("No cuenta con ninguna Jornada para finalizar.");

                        TextView dialogBtn_cancel = deleteDialogView.findViewById(R.id.btn_cancel);
                        dialogBtn_cancel.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                deleteDialog.dismiss();

                            }
                        });

                        TextView dialogBtn_okay = (TextView) deleteDialogView.findViewById(R.id.btn_ok);
                        dialogBtn_okay.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                deleteDialog.dismiss();
                            }
                        });

                        deleteDialog.show();

                    }
            }
        });
    }

    private void irSincronizarJornadaFinal(String idUsuarioJornada, String fecha, int idjornada,
                                           JSONObject ubicacionJOrnada, int idFinalizacionJornada) {
        Toast.makeText(Home.this, "Sincronizado Base de datos", Toast.LENGTH_SHORT).show();
        Endpoints endpoints = RetrofitClientInstance.getRetrofitInstance().create(Endpoints.class);


        JSONObject dataLogin = new JSONObject();
        try {
            dataLogin.put("idUsuarioJornada", idUsuarioJornada);
            dataLogin.put("fecha", fecha);
            dataLogin.put("idjornada", idjornada);
            dataLogin.put("jornadaEnCurso", 0);
            dataLogin.put("ubicacion", ubicacionJOrnada);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        Call<String> call = endpoints.updateJornada(dataLogin.toString());
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                try {

                    Toast.makeText(Home.this, "Finalizó jornada exitosamente.", Toast.LENGTH_SHORT).show();
                    String localizacionFlag = "true";
                    session.localizacionFlag(localizacionFlag);
                    session.id_jornada(String.valueOf(0));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                Toast.makeText(Home.this,
                        R.string.internet,
                        Toast.LENGTH_SHORT).show();

            }
        });

    }


    private void validarCLientesProspectos() {

        if(app.isConnected(this)){
            List<Clientes> datosSqlClientesProspecto = sqliteRoutines.readClienteProspecto();
            if (datosSqlClientesProspecto != null){

                Log.v("informacionCliente", String.valueOf(datosSqlClientesProspecto));
                for (int i = 0; i < datosSqlClientesProspecto.size(); i++) {
                    String nombreClienteProspecto = datosSqlClientesProspecto.get(i).getNombreCliente();
                    String NitoCCPorspecto = datosSqlClientesProspecto.get(i).getNitOcc();

                    String correoClienteProspecto = datosSqlClientesProspecto.get(i).getCorreoCliente();
                    String direccionClienteProspecto = datosSqlClientesProspecto.get(i).getDireccionCliente();
                    String TelefonoClienteProspecto = datosSqlClientesProspecto.get(i).getTelefonoCliente();

                    int idZonaCliente = datosSqlClientesProspecto.get(i).getIdZonaCliente();
                    int prospectoCliente = datosSqlClientesProspecto.get(i).getProspecto();
                    Double latitud = datosSqlClientesProspecto.get(i).getLatitud();
                    Double longitud = datosSqlClientesProspecto.get(i).getLongitud();
                    int idCliente = datosSqlClientesProspecto.get(i).getIdCliente();

                    JSONObject dataUbicacion = new JSONObject();

                    try {
                        dataUbicacion.put("latitud", latitud);
                        dataUbicacion.put("longitud", longitud);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }



                }
                }
        }
    }

    private void validacionDePEdidosPendientes() {

        if(app.isConnected(this)){
            List<InformacionPedidos> datosSqlinformacionPedido = sqliteRoutines.readInformacionPedido();
            if (datosSqlinformacionPedido != null){
                Log.v("informacionPedido", String.valueOf(datosSqlinformacionPedido));
                for (int i = 0; i < datosSqlinformacionPedido.size(); i++) {
                    int idClientePedido = datosSqlinformacionPedido.get(i).getIdClientePedido();
                    int idVendedorPedido = datosSqlinformacionPedido.get(i).getIdVendedorPedido();

                    int cantidadPedido = datosSqlinformacionPedido.get(i).getCantidad();
                    String descripccion = datosSqlinformacionPedido.get(i).getDescripccion();
                    String valorUnidad = datosSqlinformacionPedido.get(i).getValorUnidad();

                    Double totalPedido = datosSqlinformacionPedido.get(i).getTotalPedido();
                    String observacionpedido = datosSqlinformacionPedido.get(i).getObservacionesPedido();
                    String modoPago = datosSqlinformacionPedido.get(i).getModoPago();
                    int idPedido =datosSqlinformacionPedido.get(i).getIdPedido();

                    ArrayPedidos = new JSONObject();
                    try {
                        ArrayPedidos.put("cantidad", cantidadPedido);
                        ArrayPedidos.put("descripcion", descripccion);
                        ArrayPedidos.put("valor unidad", valorUnidad);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    arrayPedidoFinalizado.put(ArrayPedidos);

                  //  app.enviarInformacionPedido(idClientePedido,idVendedorPedido,cantidadPedido,descripccion,valorUnidad,arrayPedidoFinalizado,totalPedido,observacionpedido,modoPago,idPedido, 1, dataUbicacionPedido);
                }
            }
            }
        }

    //Funcion que permite obtener la geolocalizacion del vendedor
    private void localizacion() {

        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.ACCESS_COARSE_LOCATION}, /* Este codigo es para identificar tu request */ 1);

        ubicacion = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        if (ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) !=
                        PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION,
            }, 1000);
            return;
        }

        Location loc = ubicacion.getLastKnownLocation(LocationManager.PASSIVE_PROVIDER);

        if (loc != null) {
            latitud = loc.getLatitude();
            longitud = loc.getLongitude();
            GeolocalizacionLatitud.add(latitud);
            GeolocalizacionLongitud.add(longitud);

            dataUbicacionPedido = new JSONObject();
            try {
                dataUbicacionPedido.put("latitud", GeolocalizacionLatitud);
                dataUbicacionPedido.put("longitud", GeolocalizacionLongitud);

            } catch (JSONException e) {
                e.printStackTrace();
            }

            IniciarJornada();
            app.actualizarJornadaUsuario(idVendedor, latitud, longitud);
        }else {


            startLocationUpdates();
        }
    }

    // Trigger new location updates at interval
    protected void startLocationUpdates() {

        // Create the location request to start receiving updates
        mLocationRequest = new LocationRequest();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);


        // Create LocationSettingsRequest object using location request
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
        builder.addLocationRequest(mLocationRequest);
        LocationSettingsRequest locationSettingsRequest = builder.build();

        // Check whether location settings are satisfied
        // https://developers.google.com/android/reference/com/google/android/gms/location/SettingsClient
        SettingsClient settingsClient = LocationServices.getSettingsClient(this);
        settingsClient.checkLocationSettings(locationSettingsRequest);

        // new Google API SDK v11 uses getFusedLocationProviderClient(this)
        getFusedLocationProviderClient(this).requestLocationUpdates(mLocationRequest, new LocationCallback() {
                    @Override
                    public void onLocationResult(LocationResult locationResult) {
                        // do work here
                        onLocationChanged(locationResult.getLastLocation());
                    }
                },
                Looper.myLooper());
    }

    public void onLocationChanged(Location location) {
        // New location has now been determined
        String msg = "Updated Location: " +
                Double.toString(location.getLatitude()) + "," +
                Double.toString(location.getLongitude());

        latitud = location.getLatitude();
        longitud= location.getLongitude();

        GeolocalizacionLatitud.add(latitud);
        GeolocalizacionLongitud.add(longitud);

        dataUbicacionPedido = new JSONObject();
        try {
            dataUbicacionPedido.put("latitud", GeolocalizacionLatitud);
            dataUbicacionPedido.put("longitud", GeolocalizacionLongitud);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        if(latitud != null){
            IniciarJornada();
            app.actualizarJornadaUsuario(idVendedor, latitud, longitud);
        }else {
            LayoutInflater factory = LayoutInflater.from(Home.this);
            final View deleteDialogView = factory.inflate(R.layout.alert, null);
            final AlertDialog deleteDialog = new AlertDialog.Builder(Home.this).create();
            deleteDialog.setView(deleteDialogView);
            final TextView Title = (TextView) deleteDialogView.findViewById(R.id.txtTitle);
            Title.setText(R.string.important);
            final TextView Descripcion = (TextView) deleteDialogView.findViewById(R.id.txtDescribe);
            Descripcion.setText("No ha activado la función de ubicación en su dispositivo o verifique que haya aceptado el permiso para poder obtener la ubicación.");

            TextView dialogBtn_cancel = deleteDialogView.findViewById(R.id.btn_cancel);
            dialogBtn_cancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    deleteDialog.dismiss();

                }
            });

            TextView dialogBtn_okay = (TextView) deleteDialogView.findViewById(R.id.btn_ok);
            dialogBtn_okay.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    deleteDialog.dismiss();
                }
            });

            deleteDialog.show();
        }


    }



    //Funcion que permite iniciar la jornada del vendedor
    private void IniciarJornada() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.getDefault());
        Date date = new Date();

        fechaHoraInicioJornada = dateFormat.format(date);
        Endpoints endpoints = RetrofitClientInstance.getRetrofitInstance().create(Endpoints.class);

        final JSONObject dataUbicacionJornada = new JSONObject();
        try {
            dataUbicacionJornada.put("latitud", GeolocalizacionLatitud);
            dataUbicacionJornada.put("longitud", GeolocalizacionLongitud);

        } catch (JSONException e) {
            e.printStackTrace();
        }


        JSONObject dataInicioJornada = new JSONObject();
        try {
            dataInicioJornada.put("idUsuarioJornada", idVendedor);
            dataInicioJornada.put("ubicacion", dataUbicacionJornada);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        Call<String> call = endpoints.nuevaJornada(dataInicioJornada.toString());
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                try {

                    Log.v("aqui", String.valueOf(response));
                    JSONObject jsonArray = new JSONObject(response.body());
                    flag = true;
                    session.flag(String.valueOf(flag));
                    Log.v("flag", session.getUserDetails().get("flag"));
                    int idJornada = jsonArray.getInt("id");
                    session.id_jornada(String.valueOf(idJornada));
                    Log.v("aqui", String.valueOf(session.getUserDetails().get("id_jornada")));
                    String Mensaje = jsonArray.getString("mensaje");
                    guardarJornada(Home.this, String.valueOf(idVendedor), fechaHoraInicioJornada,
                            idJornada, dataUbicacionJornada,0.0,0.0);
                    Toast.makeText(Home.this, Mensaje,Toast.LENGTH_SHORT).show();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                Toast.makeText(Home.this,
                        R.string.internet,
                        Toast.LENGTH_SHORT).show();
            }
        });
    }




    //Funcion que guardar la informacion de la jornada en dqlite al inicar una.
    public void guardarJornada(Context aplicacion,String id_customer, String fechaHoraInicioJornada,
                               int id, JSONObject dataUbicacion,Double latitudes, Double longitudes)
    {//   ArrayList GeolocalizacionLongitudes = new ArrayList();

        Jornada jornada = new Jornada(id_customer, fechaHoraInicioJornada, id, dataUbicacion, 1, 0);
        sqliteRoutines.saveJornada(jornada);

    }
}