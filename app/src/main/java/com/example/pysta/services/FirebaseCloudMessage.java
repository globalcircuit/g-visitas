package com.example.pysta.services;

import android.Manifest;
import android.app.Activity;
import android.app.Notification;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.util.Log;
import android.widget.Toast;

import androidx.core.app.ActivityCompat;


import com.example.pysta.Aplicacion;
import com.example.pysta.Home;
import com.example.pysta.R;
import com.example.pysta.dataAccess.Endpoints;
import com.example.pysta.dataAccess.RetrofitClientInstance;
import com.example.pysta.dataAccess.SqliteRoutines;
import com.example.pysta.models.Jornada;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class FirebaseCloudMessage extends FirebaseMessagingService {
    private Context contexto;
    private Aplicacion app;
    private Notification builder;
    private SqliteRoutines sqliteRoutines;
    public static final int CHANNEL_ALERT = 1;
    public static final int CHANNEL_INFORM = 2;
    private LocationManager ubicacion;
    private Double latitud, logintud;
    public ArrayList allLocalizacionLatitud,allLocalizacionLongitud,VacioallLocalizacionLatitud,VacioallLocalizacionLongitud;
    String idUsuarioJornada,fecha;
    int idjornada,idFinalizacionJornada;
    JSONObject dataUbicaciones,dataUbicacionesNuevo;

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        contexto = getApplicationContext();
        app = (Aplicacion) getApplication();
        sqliteRoutines = new SqliteRoutines(this);
        allLocalizacionLatitud = new ArrayList();
        allLocalizacionLongitud = new ArrayList();
        VacioallLocalizacionLatitud = new ArrayList();
        VacioallLocalizacionLongitud = new ArrayList();

        ubicacion = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        if (ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(contexto, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions((Activity) contexto, new String[]{Manifest.permission.ACCESS_FINE_LOCATION,
            }, 1000);
            return;
        }

        Location loc = ubicacion.getLastKnownLocation(LocationManager.PASSIVE_PROVIDER);

        latitud = loc.getLatitude();
        logintud = loc.getLongitude();

        Intent intent = new Intent(this, ProgressIntentService.class);
        intent.setAction(Constants.ACTION_RUN_ISERVICE);
        startService(intent);
       //FinalizararJornadas();


        Log.v("Aplicacion", "From: " + remoteMessage.getFrom());

        if (sqliteRoutines.readFinalizarJornada()!= null) {

            List<Jornada> datosSqlidFinalizacionJornada = sqliteRoutines.readFinalizarJornada();
            Log.v("lista Jornada", String.valueOf(datosSqlidFinalizacionJornada));
            JSONObject ubicacion = new JSONObject();

            for (int i = 0; i < datosSqlidFinalizacionJornada.size(); i++) {

                int sincronizados = datosSqlidFinalizacionJornada.get(i).getJornadaSincronizada();

                if (sincronizados == 1) {
                    idUsuarioJornada = datosSqlidFinalizacionJornada.get(i).getIdUsuarioJornada();
                    fecha = datosSqlidFinalizacionJornada.get(i).getFecha();
                    idjornada = datosSqlidFinalizacionJornada.get(i).getIdjornada();
                    JSONObject ubicacionJOrnada = datosSqlidFinalizacionJornada.get(i).getUbicacion();
                    idFinalizacionJornada = datosSqlidFinalizacionJornada.get(i).getId();
                    String latitudes = null;
                    String Longitudes = null;

                    try {
                        latitudes = ubicacionJOrnada.getString("latitud");
                        Longitudes = ubicacionJOrnada.getString("longitud");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    if (!latitudes.equals(null)){
                        String nuevaLatiud = latitudes.replace("[", "").replace("]", "").replace(",", "");
                        String nuevaLongitud =  Longitudes.replace("[", "").replace("]", "").replace(",", "");


                        Double latitud = Double.valueOf(nuevaLatiud);
                        Double longitud = Double.valueOf(nuevaLongitud);
                        allLocalizacionLatitud.add(latitud);
                        allLocalizacionLongitud.add(longitud);
                    }else {
                        allLocalizacionLatitud.add(latitud);
                        allLocalizacionLongitud.add(logintud);

                    }



                }
            }
            Geolocalizacion();

            // btnJornada.setVisibility(View.VISIBLE);
            //btnJornadaFinal.setVisibility(View.GONE);

        }

    }

    private void Geolocalizacion() {
   //     ActivityCompat.requestPermissions((Activity) contexto, new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, /* Este codigo es para identificar tu request */ 1);

        ubicacion = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        if (ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(contexto, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions((Activity) contexto, new String[]{Manifest.permission.ACCESS_FINE_LOCATION,
            }, 1000);
            return;
        }

        Location loc = ubicacion.getLastKnownLocation(LocationManager.PASSIVE_PROVIDER);


        if (loc != null) {
            latitud = loc.getLatitude();
            logintud = loc.getLongitude();

            allLocalizacionLatitud.add(latitud);
            allLocalizacionLongitud.add(logintud);

            VacioallLocalizacionLatitud.add(latitud);
            VacioallLocalizacionLongitud.add(logintud);

            dataUbicacionesNuevo = new JSONObject();
            try {
                dataUbicacionesNuevo.put("latitud", VacioallLocalizacionLatitud);
                dataUbicacionesNuevo.put("longitud", VacioallLocalizacionLongitud);

            } catch (JSONException e) {
                e.printStackTrace();
            }


             dataUbicaciones = new JSONObject();
            try {
                dataUbicaciones.put("latitud", allLocalizacionLatitud);
                dataUbicaciones.put("longitud", allLocalizacionLongitud);

            } catch (JSONException e) {
                e.printStackTrace();
            }

           FinalizararJornada();
            app.actualizarJornadaUsuario(Integer.valueOf(idUsuarioJornada),latitud,logintud);
        }
    }



    private void FinalizararJornada() {

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
        Date date = new Date();
        final String fechaHora = dateFormat.format(date);
        Log.v("Fecha", fechaHora);

      //  Toast.makeText(Home.this, "Sincronizado Base de datos", Toast.LENGTH_SHORT).show();
        Endpoints endpoints = RetrofitClientInstance.getRetrofitInstance().create(Endpoints.class);



        JSONObject dataLogin = new JSONObject();
        try {
            dataLogin.put("idUsuarioJornada", idUsuarioJornada);
            dataLogin.put("idjornada", idjornada);
            dataLogin.put("jornadaEnCurso", 1);
            dataLogin.put("ubicacion", dataUbicaciones);


        } catch (JSONException e) {
            e.printStackTrace();
        }
        Call<String> call = endpoints.updateJornada(dataLogin.toString());
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                try {

                    Toast.makeText(contexto, "sincronización exitosa", Toast.LENGTH_SHORT).show();
                    Jornada jornada = new Jornada(idUsuarioJornada, fechaHora, idjornada, dataUbicacionesNuevo, 1, 0);
                    sqliteRoutines.saveJornada(jornada);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                Toast.makeText(contexto,
                        R.string.internet,
                        Toast.LENGTH_SHORT).show();

            }
        });

    }


    //Funcion que permite actualizar la geolocalizacion del ususario cada 15 minutos
    // en la tabla de usuarios de la bd
    private void actualizarJornadaUsuario() {

        Endpoints endpoints = RetrofitClientInstance.getRetrofitInstance().create(Endpoints.class);

        final JSONObject dataUbicacionJornada = new JSONObject();
        try {
            dataUbicacionJornada.put("latitud", latitud);
            dataUbicacionJornada.put("longitud", logintud);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        JSONObject dataInicioJornada = new JSONObject();
        try {
            dataInicioJornada.put("idUsuario", idUsuarioJornada);
            dataInicioJornada.put("ubicacion", dataUbicacionJornada);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        Call<String> call = endpoints.nuevaJornada(dataInicioJornada.toString());
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                try {

                    Log.v("actualizacion jornada", String.valueOf(response));

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                Toast.makeText(contexto,
                        R.string.internet,
                        Toast.LENGTH_SHORT).show();
            }
        });
    }


}