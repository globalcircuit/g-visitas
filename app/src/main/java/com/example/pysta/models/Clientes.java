package com.example.pysta.models;

import java.io.Serializable;

public class Clientes implements Serializable{

    String nombreCliente;
    String sucursal;
    String nitOcc;
    String correoCliente;
    String direccionCliente;
    String telefonoCliente;
    int idZonaCliente;
    int prospecto;
    int idCliente;
    Double latitud;
    Double longitud;

    public Clientes(String nombreCliente, String sucursal, String nitOcc, String correoCliente, String direccionCliente, String telefonoCliente, int idZonaCliente, int prospecto, Double latitud, Double longitud, int idCliente) {
        this.nombreCliente = nombreCliente;
        this.sucursal = sucursal;
        this.nitOcc = nitOcc;
        this.correoCliente = correoCliente;
        this.direccionCliente = direccionCliente;
        this.telefonoCliente = telefonoCliente;
        this.idZonaCliente = idZonaCliente;
        this.prospecto = prospecto;
        this.latitud = latitud;
        this.longitud = longitud;
        this.idCliente = idCliente;
    }

    public String getSucursal() {
        return sucursal;
    }

    public void setSucursal(String sucursal) {
        this.sucursal = sucursal;
    }

    public Double getLatitud() {
        return latitud;
    }

    public void setLatitud(Double latitud) {
        this.latitud = latitud;
    }

    public Double getLongitud() {
        return longitud;
    }

    public void setLongitud(Double longitud) {
        this.longitud = longitud;
    }

    public int getIdZonaCliente() {
        return idZonaCliente;
    }

    public void setIdZonaCliente(int idZonaCliente) {
        this.idZonaCliente = idZonaCliente;
    }



    public int getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(int idCliente) {
        this.idCliente = idCliente;
    }

    public String getNombreCliente() {
        return nombreCliente;
    }

    public void setNombreCliente(String nombreCliente) {
        this.nombreCliente = nombreCliente;
    }

    public String getNitOcc() {
        return nitOcc;
    }

    public void setNitOcc(String nitOcc) {
        this.nitOcc = nitOcc;
    }

    public String getCorreoCliente() {
        return correoCliente;
    }

    public void setCorreoCliente(String correoCliente) {
        this.correoCliente = correoCliente;
    }

    public String getDireccionCliente() {
        return direccionCliente;
    }

    public void setDireccionCliente(String direccionCliente) {
        this.direccionCliente = direccionCliente;
    }

    public String getTelefonoCliente() {
        return telefonoCliente;
    }

    public void setTelefonoCliente(String telefonoCliente) {
        this.telefonoCliente = telefonoCliente;
    }

    public int getProspecto() {
        return prospecto;
    }

    public void setProspecto(int prospecto) {
        this.prospecto = prospecto;
    }
}
