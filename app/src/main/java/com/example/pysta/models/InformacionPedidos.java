package com.example.pysta.models;

import com.google.gson.JsonObject;

import org.json.JSONArray;

public class InformacionPedidos {
	int idClientePedido;
	int idVendedorPedido;
	int cantidad;
	String descripccion;
	String valorUnidad;
	Double totalPedido;
	String observacionesPedido;
	String modoPago;
	int idPedido;


	public InformacionPedidos(int idClientePedido, int idVendedorPedido, int cantidad, String descripccion, String valorUnidad, Double totalPedido, String observacionesPedido, String modoPago, int idPedido) {
		this.idClientePedido = idClientePedido;
		this.idVendedorPedido = idVendedorPedido;
		this.cantidad = cantidad;
		this.descripccion = descripccion;
		this.valorUnidad = valorUnidad;
		this.totalPedido = totalPedido;
		this.observacionesPedido = observacionesPedido;
		this.modoPago = modoPago;
		this.idPedido = idPedido;
	}

	public int getIdPedido() {
		return idPedido;
	}

	public void setIdPedido(int idPedido) {
		this.idPedido = idPedido;
	}

	public int getIdClientePedido() {
		return idClientePedido;
	}

	public void setIdClientePedido(int idClientePedido) {
		this.idClientePedido = idClientePedido;
	}

	public int getIdVendedorPedido() {
		return idVendedorPedido;
	}

	public void setIdVendedorPedido(int idVendedorPedido) {
		this.idVendedorPedido = idVendedorPedido;
	}

	public int getCantidad() {
		return cantidad;
	}

	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}

	public String getDescripccion() {
		return descripccion;
	}

	public void setDescripccion(String descripccion) {
		this.descripccion = descripccion;
	}

	public String getValorUnidad() {
		return valorUnidad;
	}

	public void setValorUnidad(String valorUnidad) {
		this.valorUnidad = valorUnidad;
	}

	public Double getTotalPedido() {
		return totalPedido;
	}

	public void setTotalPedido(Double totalPedido) {
		this.totalPedido = totalPedido;
	}

	public String getObservacionesPedido() {
		return observacionesPedido;
	}

	public void setObservacionesPedido(String observacionesPedido) {
		this.observacionesPedido = observacionesPedido;
	}

	public String getModoPago() {
		return modoPago;
	}

	public void setModoPago(String modoPago) {
		this.modoPago = modoPago;
	}
}
