package com.example.pysta.models;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

public class Vendedor implements Serializable {

    int idUsuario;
    String correoUsuario;
    String rolUsuario;
    String nombreVendedor;
    String apellidoVendedor;
    String telefonoVendedor;
    String idZonaVendedor;
    String contrasenaUsuario;
    String nombreZona;

    public Vendedor(int idUsuario, String correoUsuario, String rolUsuario, String nombreVendedor, String apellidoVendedor, String telefonoVendedor, String idZonaVendedor, String contrasenaUsuario, String nombreZona) {
        this.idUsuario = idUsuario;
        this.correoUsuario = correoUsuario;
        this.rolUsuario = rolUsuario;
        this.nombreVendedor = nombreVendedor;
        this.apellidoVendedor = apellidoVendedor;
        this.telefonoVendedor = telefonoVendedor;
        this.idZonaVendedor = idZonaVendedor;
        this.contrasenaUsuario = contrasenaUsuario;
        this.nombreZona = nombreZona;
    }

    public Vendedor(JSONObject jsonObject) {
        try {
            this.idUsuario = jsonObject.getInt("idUsuario");
            this.correoUsuario = jsonObject.getString("correoUsuario");
            this.rolUsuario = jsonObject.getString("rolUsuario");
            this.nombreVendedor = jsonObject.getString("nombreVendedor");
            this.apellidoVendedor = jsonObject.getString("apellidoVendedor");
            this.telefonoVendedor = jsonObject.getString("telefonoVendedor");
            this.idZonaVendedor = jsonObject.getString("idZonaVendedor");
        } catch (JSONException e) {
            e.printStackTrace();
        }


    }

    public String getApellidoVendedor() {
        return apellidoVendedor;
    }

    public void setApellidoVendedor(String apellidoVendedor) {
        this.apellidoVendedor = apellidoVendedor;
    }

    public String getNombreZona() {
        return nombreZona;
    }

    public void setNombreZona(String nombreZona) {
        this.nombreZona = nombreZona;
    }

    public String getContrasenaUsuario() {
        return contrasenaUsuario;
    }

    public void setContrasenaUsuario(String contrasenaUsuario) {
        this.contrasenaUsuario = contrasenaUsuario;
    }

    public int getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(int idUsuario) {
        this.idUsuario = idUsuario;
    }

    public String getCorreoUsuario() {
        return correoUsuario;
    }

    public void setCorreoUsuario(String correoUsuario) {
        this.correoUsuario = correoUsuario;
    }

    public String getRolUsuario() {
        return rolUsuario;
    }

    public void setRolUsuario(String rolUsuario) {
        this.rolUsuario = rolUsuario;
    }

    public String getNombreVendedor() {
        return nombreVendedor;
    }

    public void setNombreVendedor(String nombreVendedor) {
        this.nombreVendedor = nombreVendedor;
    }

    public String getTelefonoVendedor() {
        return telefonoVendedor;
    }

    public void setTelefonoVendedor(String telefonoVendedor) {
        this.telefonoVendedor = telefonoVendedor;
    }

    public String getIdZonaVendedor() {
        return idZonaVendedor;
    }

    public void setIdZonaVendedor(String idZonaVendedor) {
        this.idZonaVendedor = idZonaVendedor;
    }
}
