package com.example.pysta.models;

import androidx.annotation.NonNull;

import org.json.JSONObject;

public class Jornada {
    String idUsuarioJornada;
    String fecha;
    int idjornada;
    JSONObject ubicacion;
    int jornadaSincronizada;
    int id;


    public Jornada(String idUsuarioJornada, String fecha, int idjornada, JSONObject ubicacion, int jornadaSincronizada, int id) {
        this.idUsuarioJornada = idUsuarioJornada;
        this.fecha = fecha;
        this.idjornada = idjornada;
        this.ubicacion = ubicacion;
        this.jornadaSincronizada = jornadaSincronizada;
        this.id = id;
    }



    @NonNull
    @Override
    public String toString() {
        JSONObject json = new JSONObject();
        try {
            json.put("idUsuarioJornada", idUsuarioJornada);
            json.put("fecha", fecha);
            json.put("idjornada", idjornada);
            json.put("ubicacion", ubicacion);
            json.put("jornadaSincronizada", jornadaSincronizada);
        } catch (Exception e) {e.printStackTrace();}
        return json.toString();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getIdUsuarioJornada() {
        return idUsuarioJornada;
    }

    public void setIdUsuarioJornada(String idUsuarioJornada) {
        this.idUsuarioJornada = idUsuarioJornada;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public int getIdjornada() {
        return idjornada;
    }

    public void setIdjornada(int idjornada) {
        this.idjornada = idjornada;
    }

    public JSONObject getUbicacion() {
        return ubicacion;
    }

    public void setUbicacion(JSONObject ubicacion) {
        this.ubicacion = ubicacion;
    }

    public int getJornadaSincronizada() {
        return jornadaSincronizada;
    }

    public void setJornadaSincronizada(int jornadaSincronizada) {
        this.jornadaSincronizada = jornadaSincronizada;
    }
}
