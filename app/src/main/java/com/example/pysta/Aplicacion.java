package com.example.pysta;

import android.Manifest;
import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;
import android.widget.Toast;

import androidx.core.app.ActivityCompat;

import com.example.pysta.dataAccess.Endpoints;
import com.example.pysta.dataAccess.RetrofitClientInstance;
import com.example.pysta.dataAccess.SqliteRoutines;
import com.example.pysta.models.Clientes;
import com.example.pysta.models.InformacionPedidos;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class Aplicacion extends Application {

    private InformacionPedidos informacionPedidos;
    private SqliteRoutines sqliteRoutines;
    boolean enviado = false;
    int id;
    private Clientes cliente;
    boolean pedido = false;
    private JSONObject dataUbicacion;
    private LocationManager ubicacion;
    private Double latitud, longitud;

    @Override
    public void onCreate() {

        sqliteRoutines = new SqliteRoutines(this);

        super.onCreate();
    }





    public boolean isConnected(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        return networkInfo != null && networkInfo.isAvailable() && networkInfo.isConnected();
    }

    public boolean enviarInformacionPedido(
            final int idClientePedido, final int idVendedor,
            final int cantidadProducto, final String nombreProductoList,
            final String valorPedidoLimpio, JSONArray arrayPedidoFinalizado,
            final Double sumaTotal, final String observacion,
            final String tipoPago, final int idPedido, final int sincronizacion, JSONObject dataUbicacion, String firmaCliente) {

        Endpoints endpoints = RetrofitClientInstance.getRetrofitInstance().create(Endpoints.class);
        JSONObject dataPedido = new JSONObject();
        try {
            dataPedido.put("idClientePedido", idClientePedido);
            dataPedido.put("idVendedorPedido", idVendedor);
            dataPedido.put("descripcion", arrayPedidoFinalizado);
            dataPedido.put("totalPedido", sumaTotal);
            dataPedido.put("observacionesPedido", observacion);
            dataPedido.put("modoPago", tipoPago);
            dataPedido.put("ubicacion", dataUbicacion);
            dataPedido.put("firma", firmaCliente);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        Call<String> call = endpoints.nuevoPedido(dataPedido.toString());
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                try {
                    Log.v("response", String.valueOf(response));
                    JSONObject jsonObject = new JSONObject(response.body());
                    String message = jsonObject.getString("message");
                    id = jsonObject.getInt("id");
                    Toast.makeText(getApplicationContext(),
                            message, Toast.LENGTH_SHORT).show();
                    pedido = true;
                    if (sincronizacion == 1){
                        sqliteRoutines.deleteInformacionPedido(idPedido);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {


                    Toast.makeText(Aplicacion.this,
                            R.string.internet,
                            Toast.LENGTH_SHORT).show();


            }
        });
        return pedido;
    }


    public boolean clienteProspecto(final String NombreCliente, final String sucursal, final String NitOcc, final String CorreoCliente,
                                    final String DireccionCliente, final String TelefonoCliente, final int zonaCliente,
                                    int prospecto , final int IdCliente , final JSONObject dataUbicacion){
        Endpoints endpoints = RetrofitClientInstance.getRetrofitInstance().create(Endpoints.class);
        JSONObject dataUpdateCliente = new JSONObject();
        try {
            dataUpdateCliente.put("nombreCliente", NombreCliente);
            dataUpdateCliente.put("sucursal", sucursal);
            dataUpdateCliente.put("nitOcc", NitOcc);
            dataUpdateCliente.put("correoCliente", CorreoCliente);
            dataUpdateCliente.put("direccionCliente", DireccionCliente);
            dataUpdateCliente.put("telefonoCliente", TelefonoCliente);
            dataUpdateCliente.put("idZonaCliente", zonaCliente);
            dataUpdateCliente.put("prospecto", prospecto);
            dataUpdateCliente.put("idCliente", IdCliente);
            dataUpdateCliente.put("ubicacion", dataUbicacion);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        Call<String> call = endpoints.updateCliente(dataUpdateCliente.toString());
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                try {
                    Log.v("response", String.valueOf(response));
                    sqliteRoutines.deleteClienteProspecto(IdCliente);
                    enviado = true;
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                Toast.makeText(Aplicacion.this,
                        R.string.internetSincronizacion,
                        Toast.LENGTH_SHORT).show();
                enviado = false;
                boolean clienteEncontrado = sqliteRoutines.buscarClienteProspecto(IdCliente);
                if(!clienteEncontrado){
                    try {
                        latitud = Double.valueOf(dataUbicacion.getString("latitud"));
                        longitud = Double.valueOf(dataUbicacion.getString("longitud"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    cliente = new Clientes(NombreCliente, sucursal, NitOcc,
                            CorreoCliente,DireccionCliente,
                            TelefonoCliente,zonaCliente,1,
                             latitud, longitud,IdCliente);
                    sqliteRoutines.saveClienteProspecto(cliente);
                }





            }
        });
        return enviado;
    }

    public JSONObject localizacion(Context context) {


        ubicacion = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions((Activity) context, new String[]{Manifest.permission.ACCESS_FINE_LOCATION,}, 1000);

        }

        Location loc = ubicacion.getLastKnownLocation(LocationManager.PASSIVE_PROVIDER);

        if(loc!=null){
            latitud = loc.getLatitude();
            longitud= loc.getLongitude();
        }

        dataUbicacion = new JSONObject();
        try {
            dataUbicacion.put("latitud", latitud);
            dataUbicacion.put("longitud", longitud);


        } catch (JSONException e) {
            e.printStackTrace();
        }
return dataUbicacion;
    }


    //Funcion que permite actualizar la geolocalizacion del ususario cada 15 minutos
    // en la tabla de usuarios de la bd
    public void actualizarJornadaUsuario(int idVendedor, Double latitudActualizar, Double longitudActualizar) {

        Endpoints endpoints = RetrofitClientInstance.getRetrofitInstance().create(Endpoints.class);

        final JSONObject dataUbicacionJornada = new JSONObject();
        try {
            dataUbicacionJornada.put("latitud", latitudActualizar);
            dataUbicacionJornada.put("longitud", longitudActualizar);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        JSONObject dataInicioJornada = new JSONObject();
        try {
            dataInicioJornada.put("idUsuario", idVendedor);
            dataInicioJornada.put("ubicacion", dataUbicacionJornada);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        Call<String> call = endpoints.actualizacionJordanaUsuario(dataInicioJornada.toString());
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                try {

                    Log.v("actualizacion jornada", String.valueOf(response));

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                Toast.makeText(Aplicacion.this,
                        R.string.internet,
                        Toast.LENGTH_SHORT).show();
            }
        });
    }

}