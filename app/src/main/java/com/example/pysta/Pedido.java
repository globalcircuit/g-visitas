package com.example.pysta;

import java.io.Serializable;

public class Pedido implements Serializable {

    int cantidad;
    int idProducto;
    String nommbrePedido;
    String valorUnitario;
    String valorTotal;


    public Pedido(int cantidad, int idProducto, String nommbrePedido, String valorUnitario, String valorTotal) {
        this.cantidad = cantidad;
        this.idProducto = idProducto;
        this.nommbrePedido = nommbrePedido;
        this.valorUnitario = valorUnitario;
        this.valorTotal = valorTotal;
    }

    public int getIdProducto() {
        return idProducto;
    }

    public void setIdProducto(int idProducto) {
        this.idProducto = idProducto;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public String getNommbrePedido() {
        return nommbrePedido;
    }

    public void setNommbrePedido(String nommbrePedido) {
        this.nommbrePedido = nommbrePedido;
    }

    public String getValorUnitario() {
        return valorUnitario;
    }

    public void setValorUnitario(String valorUnitario) {
        this.valorUnitario = valorUnitario;
    }

    public String getValorTotal() {
        return valorTotal;
    }

    public void setValorTotal(String valorTotal) {
        this.valorTotal = valorTotal;
    }
}
