package com.example.pysta;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.widget.Toast;

import com.example.pysta.dataAccess.Endpoints;
import com.example.pysta.dataAccess.RetrofitClientInstance;
import com.example.pysta.dataAccess.Session;
import com.example.pysta.dataAccess.SqliteRoutines;
import com.example.pysta.models.Vendedor;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Splash extends AppCompatActivity {

	private Aplicacion app;
	private Session session;
	private FirebaseAuth mAuth;
	private Vendedor vendedor;
	private String contraseña, email,token;
	private boolean flag = true;
	public static final String TAG = "Pysta";
	private SqliteRoutines sqliteRoutines;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_splash);

		session = new Session(getApplicationContext());
		sqliteRoutines = new SqliteRoutines(this);
		vendedor = sqliteRoutines.readVendedor();
		app = (Aplicacion) getApplicationContext();


		new Handler().postDelayed(new Runnable() {
			@Override
			public void run() {

				if (app.isConnected(Splash.this)) {
					if (vendedor != null) {
						email = vendedor.getCorreoUsuario();
						contraseña = vendedor.getContrasenaUsuario();

						flag = false;

						session.flag("false");
						Intent intent = new Intent(Splash.this, Home.class);
						startActivity(intent);
						finish();


					} else {
						Intent intent = new Intent(Splash.this, MainActivity.class);
						startActivity(intent);
						finish();
					}

				}else {
					Intent intent = new Intent(Splash.this, MainActivity.class);
					startActivity(intent);
					finish();
				}
			}
		},3000);



	}



}