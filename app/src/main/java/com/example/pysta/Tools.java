package com.example.pysta;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.AlertDialog;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Shader;
import android.media.RingtoneManager;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;

import androidx.core.app.NotificationCompat;



import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;

public class Tools {

    public static final int ID_NOTIFICACION = 1;
    public static final int ID_NOTIFICACION_FCM = 2;
    public static final int CHANNEL_ALERT = 1;
    public static final int CHANNEL_INFORM = 2;



    public static String formatNumber(int number){
        DecimalFormatSymbols dfSymbols = new DecimalFormatSymbols();
        dfSymbols.setDecimalSeparator('.');
        dfSymbols.setGroupingSeparator(',');
        DecimalFormat df = new DecimalFormat("###,###.##", dfSymbols);
        df.setGroupingSize(3);
        df.setGroupingUsed(true);
        return df.format(number);
    }

    public static String formatNumberDouble(Double number){
        DecimalFormatSymbols dfSymbols = new DecimalFormatSymbols();
        dfSymbols.setDecimalSeparator('.');
        dfSymbols.setGroupingSeparator(',');
        DecimalFormat df = new DecimalFormat("###,###.##", dfSymbols);
        df.setGroupingSize(3);
        df.setGroupingUsed(true);
        return df.format(number);
    }
}
