package com.example.pysta.dataAccess;

import okhttp3.MultipartBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

public interface Endpoints {

    // Login
    @Headers({"Content-Type: application/json; charset=utf-8"})
    @POST("usuarios/login")
    Call<String> loginUpdate(
            @Body String dataLogin
    );

    //Cliente

    // Registro de clientes nuevos
    @Headers({"Content-Type: application/json; charset=utf-8"})
    @POST("clientes/nuevoCliente")
    Call<String> registroClientesNuevos(@Body String publication);

    //Trae todos los clientes
    @GET("clientes")
    Call<String> clientes();

    // clientes por zona
    @Headers({"Content-Type: application/json; charset=utf-8"})
    @POST("clientes/getByZona")
    Call<String> getByZona(@Body String publication);

    //UpdateCliente prospecto
    @Headers({"Content-Type: application/json; charset=utf-8"})
    @POST("clientes/updateCliente")
    Call<String> updateCliente(@Body String publication);

    //Trae todos los productos
    @GET("productos")
    Call<String> productos();

    //Pedidos

    //Nuevo pedido
    @Headers({"Content-Type: application/json; charset=utf-8"})
    @POST("pedidos/nuevoPedido")
    Call<String> nuevoPedido(@Body String pedido);

    //jornada
    @Headers({"Content-Type: application/json; charset=utf-8"})
    @POST("jornada/iniciarJornada")
    Call<String> nuevaJornada(@Body String jornada);

    @Headers({"Content-Type: application/json; charset=utf-8"})
    @POST("jornada/updateJornada")
    Call<String> updateJornada(@Body String jornada);

    @Headers({"Content-Type: application/json; charset=utf-8"})
    @POST("usuarios/updateUbicacion")
    Call<String> actualizacionJordanaUsuario(@Body String jornada);

    // Visitas
    @Headers({"Content-Type: application/json; charset=utf-8"})
    @POST("visitas/nuevaVisita")
    Call<String> nuevaVista(@Body String visita);

    //Enviar firma
    @Multipart
    @POST("updateFirma")
    Call<ResponseBody> enviarFirma(@Part MultipartBody.Part file);


}