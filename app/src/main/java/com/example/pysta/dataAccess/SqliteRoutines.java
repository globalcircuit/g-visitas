package com.example.pysta.dataAccess;

import android.content.Context;
import android.database.Cursor;


import com.example.pysta.MainActivity;
import com.example.pysta.Pedido;
import com.example.pysta.Pedidos;
import com.example.pysta.models.Clientes;
import com.example.pysta.models.InformacionPedidos;
import com.example.pysta.models.Jornada;
import com.example.pysta.models.Vendedor;
import com.example.pysta.models.Visita;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class SqliteRoutines extends SqliteDatabase {

    List<InformacionPedidos> informacionPedidosArray = null;
    List<Clientes> informacionClientesProspecto = null;
    private boolean encontrado = false;
    List<Jornada> jornadaArray = null;

    public SqliteRoutines(Context context) {
        super(context);
    }


    public boolean saveVendedor(Vendedor vendedor) {
        String query;
        if (vendedor != null) {
            query = "INSERT INTO vendedor (idUsuario, correoUsuario, rolUsuario,nombreVendedor," +
                    "apellidoVendedor,telefonoVendedor,idZonaVendedor,contrasenaUsuario,nombreZona) " + "VALUES ('" +
                    vendedor.getIdUsuario() + "', '" +
                    vendedor.getCorreoUsuario() + "', '" +
                    vendedor.getRolUsuario() + "', '" +
                    vendedor.getNombreVendedor() + "', '" +
                    vendedor.getApellidoVendedor() + "', '" +
                    vendedor.getTelefonoVendedor() + "', '" +
                    vendedor.getIdZonaVendedor() + "', '" +
                    vendedor.getContrasenaUsuario() + "', '" +
                    vendedor.getNombreZona() + "')";

        } else {
            query = "DELETE FROM vendedor WHERE idUsuario = 1;";
        }
        return writeDb(query);
    }

    public boolean saveCliente(Clientes clientes) {
        String query;
        if (clientes != null) {
            query = "INSERT INTO clientes (idCliente, nombreCliente, nitOcc,direccionCliente," +
                    "telefonoCliente,correoCliente,prospecto) " + "VALUES ('" +
                    clientes.getIdCliente() + "', '" +
                    clientes.getNombreCliente() + "', '" +
                    clientes.getNitOcc() + "', '" +
                    clientes.getDireccionCliente() + "', '" +
                    clientes.getTelefonoCliente() + "', '" +
                    clientes.getCorreoCliente() + "', '" +
                    clientes.getProspecto() + "')";
        } else {
            query = "DELETE FROM clientes WHERE idCliente = 1;";
        }
        return writeDb(query);
    }

    public boolean deleteVendedor(int idVendedor){
        String query;


            query = "DELETE FROM Vendedor WHERE idUsuario = "+idVendedor+";";

        return writeDb(query);
    }

    public Vendedor readVendedor() {
        Vendedor vendedor = null;

        // Crea y ejecuta el query
        String query = "SELECT * FROM vendedor ;";
        Cursor cursor = readDb(query);

        // Verifica el cursor
        if (cursor != null) {
            try {
                if (cursor.moveToFirst()) {
                    List<Vendedor> vendedorArray  = new ArrayList<Vendedor>();

                    vendedor = new Vendedor(
                            cursor.getInt(0),
                            cursor.getString(1),
                            cursor.getString(2),
                            cursor.getString(3),
							cursor.getString(4),
                            cursor.getString(5),
                            cursor.getString(6),
                            cursor.getString(7),
                            cursor.getString(8)
                    );
                }
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                try {
                    if (!cursor.isClosed()) {
                        cursor.close();
                    }
                } catch (Exception e){
                    e.printStackTrace();
                }
            }
        }

        // Retorno
        return vendedor;
    }


    public boolean saveInformacionPedido(InformacionPedidos informacionPedidos) {
        String query;
        if (informacionPedidos != null) {
            query = "INSERT INTO informacionPedidos (idClientePedido, idVendedorPedido, cantidad,descripcion,valorUnidad,totalPedido," +
                    "observacionesPedido,modoPago) " + "VALUES ('" +
                    informacionPedidos.getIdClientePedido() + "', '" +
                    informacionPedidos.getIdVendedorPedido() + "', '" +
                    informacionPedidos.getCantidad() + "', '" +
                    informacionPedidos.getDescripccion() + "', '" +
                    informacionPedidos.getValorUnidad() + "', '" +
                    informacionPedidos.getTotalPedido() + "', '" +
                    informacionPedidos.getObservacionesPedido() + "', '" +
                    informacionPedidos.getModoPago() + "')";

        } else {
            query = "DELETE FROM vendedor WHERE idUsuario = 1;";
        }
        return writeDb(query);
    }

    public List<InformacionPedidos> readInformacionPedido() {
        InformacionPedidos informacionPedidos = null;

        // Crea y ejecuta el query
        String query = "SELECT * FROM informacionPedidos ;";
        Cursor cursor = readDb(query);

        // Verifica el cursor
        if (cursor != null) {
            int numeroOrdenes = cursor.getCount();
            try {
                if (cursor.moveToFirst()) {
                    informacionPedidosArray = new ArrayList<InformacionPedidos>(numeroOrdenes);
                    do {


                        informacionPedidos = new InformacionPedidos(
                                cursor.getInt(0),
                                cursor.getInt(1),
                                cursor.getInt(2),
                                cursor.getString(3),
                                cursor.getString(4),
                                cursor.getDouble(5),
                                cursor.getString(6),
                                cursor.getString(7),
                                cursor.getInt(8)
                        );


                        informacionPedidosArray.add(informacionPedidos);

                    } while(cursor.moveToNext());


                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            cursor.close();
        }

        // Retorno
        return informacionPedidosArray;
    }
    public boolean deleteInformacionPedido(int idInformacionPedido){
        String query;


        query = "DELETE FROM informacionPedidos WHERE idPedido = "+idInformacionPedido+";";

        return writeDb(query);
    }

    public boolean saveClienteProspecto(Clientes clientes) {
        String query ;
        if (clientes != null) {
            query = "INSERT INTO clienteProspecto (nombreCliente, nitOcc, correoCliente,direccionCliente," +
                    "telefonoCliente,idZonaCliente,prospecto,pedidoProspecto,latitud, longitud,idCliente ) " + "VALUES ('" +
                    clientes.getNombreCliente()+ "', '" +
                    clientes.getNitOcc() + "', '" +
                    clientes.getCorreoCliente() + "', '" +
                    clientes.getDireccionCliente() + "', '" +
                    clientes.getTelefonoCliente() + "', '" +
                    clientes.getIdZonaCliente() + "', '" +
                    clientes.getProspecto() + "', '" +
                    clientes.getLatitud() + "', '" +
                    clientes.getLongitud() + "', '" +
                    clientes.getIdCliente() + "')";

        } else {
            query = "DELETE FROM vendedor WHERE idUsuario = 1;";
        }
        return writeDb(query);
    }

    public List<Clientes> readClienteProspecto() {
        Clientes clientes = null;

        // Crea y ejecuta el query
        String queryclienteProspecto = "SELECT * FROM clienteProspecto ;";
        Cursor cursor = readDb(queryclienteProspecto);

        // Verifica el cursor
        if (cursor != null) {
            int numeroOrdenes = cursor.getCount();
            try {
                if (cursor.moveToFirst()) {
                    informacionClientesProspecto = new ArrayList<Clientes>(numeroOrdenes);
                    do {
                        clientes = new Clientes(
                                cursor.getString(0),
                                cursor.getString(1),
                                cursor.getString(2),
                                cursor.getString(3),
                                cursor.getString(4),
                                cursor.getString(5),
                                cursor.getInt(6),
                                cursor.getInt(7),
                                cursor.getDouble(8),
                                cursor.getDouble(9),
                                cursor.getInt(10)

                        );


                        informacionClientesProspecto.add(clientes);

                    } while(cursor.moveToNext());


                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            cursor.close();
        }

        // Retorno
        return informacionClientesProspecto;
    }

    public boolean buscarClienteProspecto(int idCliente) {
        Clientes clientes = null;

            // Crea y ejecuta el query
        String query = "SELECT * FROM clienteProspecto WHERE idCliente = "+idCliente+";";
        Cursor cursor = readDb(query);

        // Verifica el cursor
        if (cursor != null) {

            int numeroOrdenes = cursor.getCount();
            try {
                if (cursor.moveToFirst()) {
                    encontrado = true;
                    informacionClientesProspecto = new ArrayList<Clientes>(numeroOrdenes);
                    do {


                        clientes = new Clientes(
                                cursor.getString(0),
                                cursor.getString(1),
                                cursor.getString(2),
                                cursor.getString(3),
                                cursor.getString(4),
                                cursor.getString(5),
                                cursor.getInt(6),
                                cursor.getInt(7),
                                cursor.getDouble(8),
                                cursor.getDouble(9),
                                cursor.getInt(10)
                        );


                        informacionClientesProspecto.add(clientes);

                    } while(cursor.moveToNext());


                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            cursor.close();
        }

        // Retorno
        return encontrado;
    }

    public boolean deleteClienteProspecto(int idCliente){
        String query;


        query = "DELETE FROM clienteProspecto WHERE idCliente = "+idCliente+";";

        return writeDb(query);
    }

    public boolean saveJornada(Jornada jornada) {
        String query;
        if (jornada != null) {
            query = "INSERT INTO jornada (idUsuarioJornada, fecha, idjornada,ubicacion,jornadaSincronizada) " +
                    "VALUES ('" +
                    jornada.getIdUsuarioJornada() + "', '" +
                    jornada.getFecha() + "', '" +
                    jornada.getIdjornada() + "', '" +
                    jornada.getUbicacion() + "', '" +
                    jornada.getJornadaSincronizada()  + "')";

        } else {
            query = "DELETE FROM clientes WHERE id = 1;";
        }
        return writeDb(query);
    }

    public List<Jornada> readFinalizarJornada() {
        Jornada jornada = null;

        // Crea y ejecuta el query
        String query = "SELECT * FROM jornada WHERE jornadaSincronizada = 1;";
        Cursor cursor = readDb(query);

        // Verifica el cursor
        if (cursor != null) {
            int numeroOrdenes = cursor.getCount();
            try {
                if (cursor.moveToFirst()) {
                    jornadaArray = new ArrayList<Jornada>(numeroOrdenes);
                    do {


                        String idUsuarioJornada = cursor.getString(0);
                        String fecha = cursor.getString(1);
                        int idjornada = cursor.getInt(2);
                        String ubicacion = cursor.getString(3);
                        int jornadaSincronizada = cursor.getInt(4);
                        int id = cursor.getInt(5);

                        JSONObject ubicaciones=new JSONObject(ubicacion);
                        Jornada jornada1 = new Jornada(
                                idUsuarioJornada,
                                fecha,
                                idjornada,
                                ubicaciones,
                                jornadaSincronizada,
                                id
                        );


                        jornadaArray.add(jornada1);

                    } while(cursor.moveToNext());


                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            cursor.close();
        }

        // Retorno
        return jornadaArray;
    }

    public boolean deleteSincronizacionJornada(int id) {
        String query;

        query = "DELETE FROM jornada WHERE id = "+id+";";
        return writeDb(query);
    }


    public boolean saveVisitas(Visita vista) {
        String query;
        if (vista != null) {
            query = "INSERT INTO visitas (idUsuario, idCliente, fechaHoraInicio,fechaHoraFin, latitud,longitud) " +
                    "VALUES ('" +
                    vista.getIdUsuario() + "', '" +
                    vista.getIdCliente() + "', '" +
                    vista.getFechaHoraInicio() + "', '" +
                    vista.getFechaHoraFin() + "', '" +
                    vista.getLatitud() + "', '" +
                    vista.getLongitud()  + "')";

        } else {
            query = "DELETE FROM visitas WHERE id = 1;";
        }
        return writeDb(query);
    }
}