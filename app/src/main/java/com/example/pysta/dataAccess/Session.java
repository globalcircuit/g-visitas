package com.example.pysta.dataAccess;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import androidx.appcompat.app.AppCompatActivity;


import com.example.pysta.MainActivity;

import java.util.HashMap;

/**
 * Created by infotechs on 18/04/2018.
 */
public class Session extends AppCompatActivity {
    SharedPreferences pref;

    // Editor for Shared preferences
    SharedPreferences.Editor editor;

    // Context
    Context _context;

    // Shared pref mode
    int PRIVATE_MODE = 0;

    // Sharedpref file name
    private static final String PREF_NAME = "session";

    // All Shared Preferences Keys
    private static final String IS_LOGIN = "IsLoggedIn";

    // User name (make variable public to access from outside)
    public static final String KEY_FIRSNAME = "firsname";
    public static final String KEY_IDCUSTOMER = "id_customer";
    public static final String KEY_LASTNAME = "lastname";
    public static final String KEY_EMAIL = "email";
    public static final String KEY_PASSWORD = "password";
    public static final String KEY_Token = "token";
    public static final String KEY_CUSTOMER = "customer";
    public static final String KEY_FLAG = "flag";
    public static final String KEY_IDJORNADA = "id_jornada";
    public static final String KEY_lOCALIZACIONFLAG = "localizacionFlag";





    // Constructor
    public Session(Context context) {
        this._context = context;
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }

    /**
     * Create login session
     */
    public void createLoginSession(String firsname, String lastname, String phone, String address, String idUser, String email) {
        // Storing login value as TRUE


        // Storing name in pref
        editor.putString(KEY_FIRSNAME, firsname);
        editor.putString(KEY_LASTNAME, lastname);
        editor.putString(KEY_EMAIL, email);
        // Storing email in pref

        // commit changes
        editor.commit();
    }

    public void id_customer(String id_customer) {

        // Storing name in pref
        editor.putString(KEY_IDCUSTOMER, id_customer);

        // Storing email in pref


        // commit changes
        editor.commit();
    }

    public void email(String email) {

        // Storing name in pref
        editor.putString(KEY_EMAIL, email);

        // Storing email in pref


        // commit changes
        editor.commit();
    }

    public void localizacionFlag(String localizacionFlag) {

        // Storing name in pref
        editor.putString(KEY_lOCALIZACIONFLAG, localizacionFlag);

        // Storing email in pref


        // commit changes
        editor.commit();
    }

    public void flag(String flag) {

        // Storing name in pref
        editor.putString(KEY_FLAG, flag);

        // Storing email in pref


        // commit changes
        editor.commit();
    }

    public boolean login() {

        // Storing name in pref
        editor.putBoolean(IS_LOGIN, true);


        // Storing email in pref


        // commit changes
        editor.commit();

        return true;
    }

    public void logout() {

        // Storing name in pref
        editor.putBoolean(IS_LOGIN, false);


        // Storing email in pref


        // commit changes
        editor.commit();
    }


    public void customer(String customer) {

        // Storing name in pref
        editor.putString(KEY_CUSTOMER, customer);


        // Storing email in pref


        // commit changes
        editor.commit();
    }

    public void password(String password) {

        // Storing name in pref
        editor.putString(KEY_PASSWORD, password);

        // Storing email in pref


        // commit changes
        editor.commit();
    }

    public void id_jornada(String id_jornada) {

        // Storing name in pref
        editor.putString(KEY_IDJORNADA, id_jornada);

        // Storing email in pref


        // commit changes
        editor.commit();
    }

    public void firsname(String firsname) {

        // Storing name in pref
        editor.putString(KEY_FIRSNAME, firsname);

        // Storing email in pref


        // commit changes
        editor.commit();
    }

    public void token(String token) {

        // Storing name in pref
        editor.putString(KEY_Token, token);

        // Storing email in pref


        // commit changes
        editor.commit();
    }



    /**
     * Check login method wil check user login status If false it will redirect
     * user to login page Else won't do anything
     */
    /*
    public void checkLogin() {
        // Check login status
        if (!this.isLoggedIn()) {
            // user is not logged in redirect him to Login Activity
            Intent i = new Intent(_context, Customer.class);
            // Closing all the Activities
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

            // Add new Flag to start new Activity
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

            // Staring Login Activity
            _context.startActivity(i);
        }

    }

     */

    /**
     * Get stored session data
     */
    public HashMap<String, String> getUserDetails() {
        HashMap<String, String> user = new HashMap<String, String>();
        // user name
        user.put(KEY_FIRSNAME, pref.getString(KEY_FIRSNAME, null));
        user.put(KEY_LASTNAME, pref.getString(KEY_LASTNAME, null));

        user.put(KEY_EMAIL, pref.getString(KEY_EMAIL, null));
        user.put(KEY_CUSTOMER, pref.getString(KEY_CUSTOMER, null));

        user.put(KEY_IDCUSTOMER, pref.getString(KEY_IDCUSTOMER, null));
        user.put(KEY_PASSWORD, pref.getString(KEY_PASSWORD, null));
        user.put(KEY_Token, pref.getString(KEY_Token, null));
        user.put(KEY_FLAG, pref.getString(KEY_FLAG, null));
        user.put(KEY_IDJORNADA, pref.getString(KEY_IDJORNADA, null));
        user.put(KEY_lOCALIZACIONFLAG, pref.getString(KEY_lOCALIZACIONFLAG, null));
        // user email id
        //user.put(KEY_EMAIL, pref.getString(KEY_EMAIL, null));

        // return user
        return user;
    }

    /**
     * Clear session details
     */
    public void logoutUser() {
        // Clearing all data from Shared Preferences
        editor.clear();
        editor.commit();

        // After logout redirect user to Loing Activity
        Intent i = new Intent(_context, MainActivity.class);
        // Closing all the Activities
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        // Add new Flag to start new Activity
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        // Staring Login Activity
        _context.startActivity(i);
        finish();

    }

    /**
     * Quick check for login
     **/
    // Get Login State
    public boolean isLoggedIn() {
        return pref.getBoolean(IS_LOGIN, false);
    }

}