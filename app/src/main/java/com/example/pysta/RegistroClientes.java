package com.example.pysta;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Looper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.example.pysta.dataAccess.Endpoints;
import com.example.pysta.dataAccess.RetrofitClientInstance;
import com.example.pysta.dataAccess.SqliteRoutines;
import com.example.pysta.models.Vendedor;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.SettingsClient;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.google.android.gms.location.LocationServices.getFusedLocationProviderClient;

public class RegistroClientes extends AppCompatActivity {

    private EditText editTextNombreCliente,editTextDireccion,editTextCorreo,editTextNitCedula,editTextTelefono,editTextBarrio;
    private String nombreCliente,direccionCliente,correoCliente,nitOcc,telefonoCliente,barrioCliente;
    private int idZonaCliente;
    private Button btnRegistroCliente;
    private ImageButton btnBackRegistroCliente;
    private LocationManager ubicacion;
    public ArrayList GeolocalizacionLatitud = new ArrayList();
    public ArrayList GeolocalizacionLongitud = new ArrayList();
    private SqliteRoutines sqliteRoutines;
    private Vendedor vendedor;
    private String idZonavendedor;
    private  Double latitud, longintud;
    private LocationRequest mLocationRequest;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro_clientes);

        sqliteRoutines = new SqliteRoutines(this);
        vendedor = sqliteRoutines.readVendedor();
        idZonaCliente = Integer.parseInt(vendedor.getIdZonaVendedor());

        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, /* Este codigo es para identificar tu request */ 1);


        if (ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION,
            }, 1000);

        }

        editTextNombreCliente = findViewById(R.id.editTextNombreCliente);
        editTextDireccion = findViewById(R.id.editTextDireccion);
        editTextBarrio = findViewById(R.id.editTextBarrio);
        editTextCorreo = findViewById(R.id.editTextCorreo);
        editTextNitCedula = findViewById(R.id.editTextNitCedula);
        editTextTelefono = findViewById(R.id.editTextTelefono);
        btnRegistroCliente = findViewById(R.id.btnRegistroCliente);
        btnBackRegistroCliente = findViewById(R.id.btnBackRegistroCliente);


        btnBackRegistroCliente.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(RegistroClientes.this, Home.class);
                startActivity(intent);
                finish();
            }
        });

        btnRegistroCliente.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                localizacion();
                nombreCliente = editTextNombreCliente.getText().toString();
                direccionCliente = editTextDireccion.getText().toString();
                correoCliente = editTextCorreo.getText().toString();
                nitOcc = editTextNitCedula.getText().toString();
                telefonoCliente = editTextTelefono.getText().toString();
                barrioCliente = editTextBarrio.getText().toString();
                if (
                        nombreCliente.equals("") && direccionCliente.equals("")&&  correoCliente.equals("")&&
                        nitOcc.equals("")&& telefonoCliente.equals("")&& barrioCliente.equals("")
                ){
                    Toast.makeText(RegistroClientes.this, R.string.allCampos,
                            Toast.LENGTH_LONG).show();

                }else if(nombreCliente.equals("") ){
                    Toast.makeText(RegistroClientes.this, R.string.emptyNombreCliente,
                            Toast.LENGTH_LONG).show();

                }else if(direccionCliente.equals("") ){
                    Toast.makeText(RegistroClientes.this, R.string.emptyDireccionCliente,
                            Toast.LENGTH_LONG).show();

                }else if(barrioCliente.equals("") ){
                    Toast.makeText(RegistroClientes.this, R.string.emptyBarrioCliente,
                            Toast.LENGTH_LONG).show();

                }else if(correoCliente.equals("") ){
                    Toast.makeText(RegistroClientes.this, R.string.emptyCorreoCliente,
                            Toast.LENGTH_LONG).show();

                }else if(nitOcc.equals("") ){
                    Toast.makeText(RegistroClientes.this, R.string.emptyNitoCc,
                            Toast.LENGTH_LONG).show();

                }else if(telefonoCliente.equals("")){
                    Toast.makeText(RegistroClientes.this, R.string.emptyTelefono,
                            Toast.LENGTH_LONG).show();
                }else if(idZonaCliente == 0){
                    Toast.makeText(RegistroClientes.this, R.string.emptyZona,
                            Toast.LENGTH_LONG).show();
                }else if(GeolocalizacionLatitud.size() == 0){
                    LayoutInflater factory = LayoutInflater.from(RegistroClientes.this);
                    final View deleteDialogView = factory.inflate(R.layout.alert, null);
                    final AlertDialog deleteDialog = new AlertDialog.Builder(RegistroClientes.this).create();
                    deleteDialog.setView(deleteDialogView);
                    final TextView Title = (TextView) deleteDialogView.findViewById(R.id.txtTitle);
                    Title.setText(R.string.important);
                    final TextView Descripcion = (TextView) deleteDialogView.findViewById(R.id.txtDescribe);
                    Descripcion.setText("No ha activado la función de ubicación en su dispositivo o verifique que haya aceptado el permiso para poder obtener la ubicación.");

                    TextView dialogBtn_cancel = deleteDialogView.findViewById(R.id.btn_cancel);
                    dialogBtn_cancel.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            deleteDialog.dismiss();

                        }
                    });

                    TextView dialogBtn_okay = (TextView) deleteDialogView.findViewById(R.id.btn_ok);
                    dialogBtn_okay.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            startLocationUpdates();
                            deleteDialog.dismiss();
                        }
                    });

                    deleteDialog.show();
                }else {
                    RegistrarCliente();

                }
            }
        });
    }

    private void localizacion() {

        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, /* Este codigo es para identificar tu request */ 1);

        ubicacion = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        if (ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION,
            }, 1000);
            return;
        }

        Location loc = ubicacion.getLastKnownLocation(LocationManager.PASSIVE_PROVIDER);

        if (loc != null) {
             latitud = loc.getLatitude();
             longintud = loc.getLongitude();
            GeolocalizacionLatitud.add(String.valueOf(latitud));
            GeolocalizacionLongitud.add(String.valueOf(longintud));

        }else {
            startLocationUpdates();
        }
    }

    protected void startLocationUpdates() {

        // Create the location request to start receiving updates
        mLocationRequest = new LocationRequest();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);


        // Create LocationSettingsRequest object using location request
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
        builder.addLocationRequest(mLocationRequest);
        LocationSettingsRequest locationSettingsRequest = builder.build();

        // Check whether location settings are satisfied
        // https://developers.google.com/android/reference/com/google/android/gms/location/SettingsClient
        SettingsClient settingsClient = LocationServices.getSettingsClient(this);
        settingsClient.checkLocationSettings(locationSettingsRequest);

        // new Google API SDK v11 uses getFusedLocationProviderClient(this)
        getFusedLocationProviderClient(this).requestLocationUpdates(mLocationRequest, new LocationCallback() {
                    @Override
                    public void onLocationResult(LocationResult locationResult) {
                        // do work here
                        onLocationChanged(locationResult.getLastLocation());
                    }
                },
                Looper.myLooper());
    }

    public void onLocationChanged(Location location) {
        // New location has now been determined
        String msg = "Updated Location: " +
                Double.toString(location.getLatitude()) + "," +
                Double.toString(location.getLongitude());

        latitud = location.getLatitude();
        longintud= location.getLongitude();

        GeolocalizacionLatitud.add(latitud);
        GeolocalizacionLongitud.add(longintud);

        GeolocalizacionLatitud.add(String.valueOf(latitud));
        GeolocalizacionLongitud.add(String.valueOf(longintud));




    }

    //Metodo para realizar el registro del cliente al server
    private void RegistrarCliente(){

        JSONObject dataUbicacion = new JSONObject();
        try {
            dataUbicacion.put("latitud", latitud);
            dataUbicacion.put("longitud", longintud);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        Endpoints endpoints = RetrofitClientInstance.getRetrofitInstance().create(Endpoints.class);
        JSONObject dataRegistroCliente = new JSONObject();
        try {
            dataRegistroCliente.put("nombreCliente", nombreCliente);
            dataRegistroCliente.put("nitOcc", nitOcc);
            dataRegistroCliente.put("correoCliente", correoCliente);
            dataRegistroCliente.put("direccionCliente", direccionCliente+ " " + barrioCliente);
            dataRegistroCliente.put("telefonoCliente", telefonoCliente);
            dataRegistroCliente.put("idZonaCliente", idZonaCliente);
            dataRegistroCliente.put("ubicacion", dataUbicacion);


        } catch (JSONException e) {
            e.printStackTrace();
        }
        Call<String> call = endpoints.registroClientesNuevos(dataRegistroCliente.toString());
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                try {
                    JSONObject jsonObject = new JSONObject(response.body());
                    Log.v("response", String.valueOf(jsonObject));
                    String mensaje = jsonObject.getString("message");

                    Toast.makeText(RegistroClientes.this, mensaje, Toast.LENGTH_SHORT).show();

                    Intent intent = new Intent(RegistroClientes.this, Home.class);
                    startActivity(intent);
                    finish();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                Toast.makeText(RegistroClientes.this,
                        R.string.internet,
                        Toast.LENGTH_SHORT).show();
            }
        });
    }

}
