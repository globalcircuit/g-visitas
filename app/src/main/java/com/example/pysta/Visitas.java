package com.example.pysta;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.content.Intent;
import android.graphics.Color;
import android.location.Location;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.pysta.dataAccess.Endpoints;
import com.example.pysta.dataAccess.RetrofitClientInstance;
import com.example.pysta.dataAccess.SqliteRoutines;
import com.example.pysta.models.Clientes;
import com.example.pysta.models.Vendedor;
import com.example.pysta.models.Visita;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.view.View.GONE;

public class Visitas extends AppCompatActivity {
	private JSONArray jsonArrayClientes;
	private JSONArray clientesArray = new JSONArray();
	private List<String> listaClientes = new ArrayList<>();
	private AutoCompleteTextView actv;
	private int idClientePedido = 0;
	private int zonaCliente,idVendedor,zonaVender;
	private Clientes informacionClientes;
	private ProgressBar simpleProgressBar;
	private LinearLayout linearPrincipal;
	private Button btnEnviarvisita;
	private ImageButton btnBackVisita;
	private Aplicacion app;
	private JSONObject dataUbicacion = new JSONObject();
	private SqliteRoutines sqliteRoutines;
	private Vendedor vendedor;
	private boolean flagLocaliacionCliente, flagInformacionGeolocalizacionCliente,certificaCliente;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_visitas);
		sqliteRoutines = new SqliteRoutines(this);
		sqliteRoutines = new SqliteRoutines(this);
		vendedor = sqliteRoutines.readVendedor();
		idVendedor = vendedor.getIdUsuario();
		zonaVender = Integer.valueOf(vendedor.getIdZonaVendedor());
		obtenerClientes();

		app = (Aplicacion) getApplicationContext();
		dataUbicacion = app.localizacion(Visitas.this);
		linearPrincipal = findViewById(R.id.linearPrincipal);
		btnEnviarvisita = findViewById(R.id.btnEnviarvisita);
		simpleProgressBar = findViewById(R.id.progressBar);
		btnBackVisita = findViewById(R.id.btnBackVisita);
		simpleProgressBar . setProgress ( 90 );
		linearPrincipal.setVisibility(GONE);

		//Getting the instance of AutoCompleteTextView
		actv = (AutoCompleteTextView) findViewById(R.id.autoCompleteTextView);
		actv.setThreshold(1);//will start working from first character

		actv.setTextColor(getResources().getColor(R.color.colorsubText));

		actv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				String clienteSeleccionado = String.valueOf(parent.getItemAtPosition(position));
				Log.v("Palabra", String.valueOf(parent.getItemAtPosition(position)));
				String latitudCliente = null;
				String longitudCliente = null;
				for (int i = 0; i < jsonArrayClientes.length(); i++) {
					JSONObject jsonObjectClientes = null;
					try {
						jsonObjectClientes = jsonArrayClientes.getJSONObject(i);

						String BuscarNombreCliente = jsonObjectClientes.getString("sucursal");
						if (clienteSeleccionado.equals(BuscarNombreCliente)) {
							idClientePedido = jsonObjectClientes.getInt("idCliente");
							zonaCliente = jsonObjectClientes.getInt("idZonaCliente");

							String ubicaciones = jsonObjectClientes.getString("ubicacionCliente");
							if (ubicaciones != "null") {

								JSONObject ubicacionesCliente = jsonObjectClientes.getJSONObject("ubicacionCliente");

								latitudCliente = ubicacionesCliente.getString("latitud");
								longitudCliente = ubicacionesCliente.getString("longitud");
							}else {
								latitudCliente = "0.0";
								longitudCliente = "0.0";
							}

							informacionClientes = new Clientes(
									jsonObjectClientes.getString("nombreCliente"),
									jsonObjectClientes.getString("sucursal"),
									jsonObjectClientes.getString("nitOcc"),
									jsonObjectClientes.getString("correoCliente"),
									jsonObjectClientes.getString("direccionCliente"),
									jsonObjectClientes.getString("telefonoCliente"),
									jsonObjectClientes.getInt("idZonaCliente"),
									jsonObjectClientes.getInt("prospecto"),
									Double.valueOf(latitudCliente),
									Double.valueOf(longitudCliente),
									jsonObjectClientes.getInt("idCliente")
							);

							if (Double.valueOf(latitudCliente) != 0.0){
								flagLocaliacionCliente = localizacionCliente();
							}else {
								flagInformacionGeolocalizacionCliente = true;
							}
						}
					} catch (JSONException e) {
						e.printStackTrace();
					}
				}
			}
		});

		btnEnviarvisita.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {

				if (idClientePedido == 0) {
					Toast.makeText(getApplicationContext(),
							R.string.selecCliente, Toast.LENGTH_SHORT).show();
				}else if(dataUbicacion.length()== 0){
					LayoutInflater factory = LayoutInflater.from(Visitas.this);
					final View deleteDialogView = factory.inflate(R.layout.alert, null);
					final AlertDialog deleteDialog = new AlertDialog.Builder(Visitas.this).create();
					deleteDialog.setView(deleteDialogView);
					final TextView Title = (TextView) deleteDialogView.findViewById(R.id.txtTitle);
					Title.setText(R.string.important);
					final TextView Descripcion = (TextView) deleteDialogView.findViewById(R.id.txtDescribe);
					Descripcion.setText("No ha activado la función de ubicación en su dispositivo o verifique que haya aceptado el permiso para poder obtener la ubicación.");

					TextView dialogBtn_cancel = deleteDialogView.findViewById(R.id.btn_cancel);
					dialogBtn_cancel.setOnClickListener(new View.OnClickListener() {
						@Override
						public void onClick(View v) {
							deleteDialog.dismiss();

						}
					});

					TextView dialogBtn_okay = (TextView) deleteDialogView.findViewById(R.id.btn_ok);
					dialogBtn_okay.setOnClickListener(new View.OnClickListener() {
						@Override
						public void onClick(View v) {

							deleteDialog.dismiss();
						}
					});

					deleteDialog.show();
				}else {
					if (flagInformacionGeolocalizacionCliente){
						LayoutInflater factory = LayoutInflater.from(Visitas.this);
						final View deleteDialogView = factory.inflate(R.layout.alertactualizacion, null);
						final AlertDialog deleteDialog = new AlertDialog.Builder(Visitas.this).create();
						deleteDialog.setView(deleteDialogView);
						final TextView Title = (TextView) deleteDialogView.findViewById(R.id.txtTitle);
						Title.setText(R.string.important);
						final TextView Descripcion = (TextView) deleteDialogView.findViewById(R.id.txtDescribe);
						Descripcion.setText(R.string.geolocaliazcionClienenull);

						TextView dialogBtn_cancel = deleteDialogView.findViewById(R.id.btn_NotActualizar);
						dialogBtn_cancel.setOnClickListener(new View.OnClickListener() {
							@Override
							public void onClick(View v) {
								deleteDialog.dismiss();
								enviarVisita();
							}
						});

						TextView dialogBtn_okay = (TextView) deleteDialogView.findViewById(R.id.btn_Actualizar);
						dialogBtn_okay.setOnClickListener(new View.OnClickListener() {
							@Override
							public void onClick(View v) {

								deleteDialog.dismiss();
								enviarVisita();
								actualizarInformacionCLiente();
							}
						});

						deleteDialog.show();
					}else {
						if(flagLocaliacionCliente){
							enviarVisita();
						}else {
							LayoutInflater factory = LayoutInflater.from(Visitas.this);
							final View deleteDialogView = factory.inflate(R.layout.alert, null);
							final AlertDialog deleteDialog = new AlertDialog.Builder(Visitas.this).create();
							deleteDialog.setView(deleteDialogView);
							final TextView Title = (TextView) deleteDialogView.findViewById(R.id.txtTitle);
							Title.setText(R.string.important);
							final TextView Descripcion = (TextView) deleteDialogView.findViewById(R.id.txtDescribe);
							Descripcion.setText(R.string.geolocaliazcionClieneFailVisita);

							TextView dialogBtn_cancel = deleteDialogView.findViewById(R.id.btn_cancel);
							dialogBtn_cancel.setOnClickListener(new View.OnClickListener() {
								@Override
								public void onClick(View v) {
									deleteDialog.dismiss();

								}
							});

							TextView dialogBtn_okay = (TextView) deleteDialogView.findViewById(R.id.btn_ok);
							dialogBtn_okay.setOnClickListener(new View.OnClickListener() {
								@Override
								public void onClick(View v) {

									deleteDialog.dismiss();
									enviarVisita();

								}
							});

							deleteDialog.show();
						}
					}


				}
			}
		});

		btnBackVisita.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(Visitas.this, Home.class);
				startActivity(intent);
				finish();
			}
		});
		
	}

	private void actualizarInformacionCLiente() {

				if(informacionClientes.getLatitud()==0.0){

					app.clienteProspecto(informacionClientes.getNombreCliente(),informacionClientes.getSucursal(), informacionClientes.getNitOcc(),
							informacionClientes.getCorreoCliente(),informacionClientes.getDireccionCliente(),
							informacionClientes.getTelefonoCliente(),zonaCliente,informacionClientes.getProspecto(),
							informacionClientes.getIdCliente(), dataUbicacion);
				}


	}

	//Funcion que permte optener la distancia entre el cliente y la posicion del vendedor.
	private boolean localizacionCliente() {
		boolean	flaGeolocalizacionCliente = true;
		double distMax = 100.0;
		Location cliente = new Location("localizacionCliente");
		cliente.setLatitude(informacionClientes.getLatitud());
		cliente.setLongitude(informacionClientes.getLongitud());

		Location localizacionGPS =new Location("localizacionGPS");
		try {
			localizacionGPS.setLatitude(Double.parseDouble(dataUbicacion.getString("latitud")));
			localizacionGPS.setLongitude(Double.parseDouble(dataUbicacion.getString("longitud")));
		} catch (JSONException e) {
			e.printStackTrace();
		}


		float distance = cliente.distanceTo(localizacionGPS);

		if( distance <= distMax ){
			System.out.println("La distancia es menor");
			flaGeolocalizacionCliente = true;
		}else {
			System.out.println("La distancia es mayor");
			flaGeolocalizacionCliente = false;
		}

		return flaGeolocalizacionCliente;
	}

	private void enviarVisita() {
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
		Date date = new Date();

		final String fechaHoraInicio = dateFormat.format(date);
		Log.v("Fecha", fechaHoraInicio);
		Endpoints endpoints = RetrofitClientInstance.getRetrofitInstance().create(Endpoints.class);
		JSONObject dataVisita = new JSONObject();
		try {
			dataVisita.put("id_vendedor_visita", idVendedor);
			dataVisita.put("id_cliente_visita", idClientePedido);
			dataVisita.put("fechaHoraInicio", fechaHoraInicio);
			dataVisita.put("fechaHoraFin", fechaHoraInicio);
			dataVisita.put("ubicacion", dataUbicacion);
			dataVisita.put("tipo", "");
		} catch (JSONException e) {
			e.printStackTrace();
		}
		Call<String> call = endpoints.nuevaVista(dataVisita.toString());
		call.enqueue(new Callback<String>() {
			@Override
			public void onResponse(Call<String> call, Response<String> response) {
				try {

					Log.v("response", String.valueOf(response));
					JSONObject jsonObject = new JSONObject(response.body());
					String message = jsonObject.getString("message");

					Toast.makeText(getApplicationContext(),
							message, Toast.LENGTH_SHORT).show();

					Intent intent = new Intent(Visitas.this, Home.class);
					startActivity(intent);
					finish();

				} catch (Exception e) {
					e.printStackTrace();
				}
			}

			@Override
			public void onFailure(Call<String> call, Throwable t) {
				Toast.makeText(Visitas.this,
						R.string.internet,
						Toast.LENGTH_SHORT).show();


				}


		});
	}

	private void obtenerClientes() {
		Endpoints endpoints = RetrofitClientInstance.getRetrofitInstance().create(Endpoints.class);
		JSONObject dataZonaCliente = new JSONObject();
		try {
			dataZonaCliente.put("idZonaCliente", zonaVender);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		Call<String> call = endpoints.getByZona(dataZonaCliente.toString());
		call.enqueue(new Callback<String>() {
			@Override
			public void onResponse(Call<String> call, Response<String> response) {
				try {

					jsonArrayClientes = new JSONArray(response.body());
					for (int i = 0; i < jsonArrayClientes.length(); i++) {
						JSONObject jsonObjectClientes = jsonArrayClientes.getJSONObject(i);

						Clientes clientes = new Clientes(
								jsonObjectClientes.getString("nombreCliente"),
								jsonObjectClientes.getString("sucursal"),
								jsonObjectClientes.getString("nitOcc"),
								jsonObjectClientes.getString("correoCliente"),
								jsonObjectClientes.getString("direccionCliente"),
								jsonObjectClientes.getString("telefonoCliente"),
								jsonObjectClientes.getInt("idZonaCliente"),
								jsonObjectClientes.getInt("prospecto"),
								0.0,0.0,
								jsonObjectClientes.getInt("idCliente")
						);

						String nombreCliente = jsonObjectClientes.getString("sucursal");

						clientesArray.put(clientes);
						Log.v("Cliente", String.valueOf(clientesArray));
						listaClientes.add(nombreCliente);

						ArrayAdapter<String> adapter = new ArrayAdapter<String>(Visitas.this,
								R.layout.producto, R.id.autoCompleteItem, listaClientes);

						
						actv.setAdapter(adapter);
					}
					simpleProgressBar.setVisibility(GONE);
					linearPrincipal.setVisibility(View.VISIBLE);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}

			@Override
			public void onFailure(Call<String> call, Throwable t) {
				Toast.makeText(Visitas.this,
						R.string.internet,
						Toast.LENGTH_SHORT).show();
			}
		});
	}
}