package com.example.pysta;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.pysta.dataAccess.Endpoints;
import com.example.pysta.dataAccess.RetrofitClientInstance;
import com.example.pysta.dataAccess.Session;
import com.example.pysta.dataAccess.SqliteRoutines;
import com.example.pysta.models.Vendedor;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.gson.JsonArray;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    private Button btnLogin;
    private EditText edtEmail,edtContraseña;
    private String contraseña, email,correoUsuario,rolUsuario,nombreVendedor,apellidoVendedor,telefonoVendedor,
            idZonaVendedor,contrasenaUsuario,nombreZona;
    private int idUsuario;
    private Vendedor vendedor;
    private SqliteRoutines sqliteRoutines;
    private boolean flag = true;
    private  String token;
    private Aplicacion app;
    private Session session;
    private FirebaseAuth mAuth;
    public static final String TAG = "Pysta";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        session = new Session(getApplicationContext());
        app = (Aplicacion) getApplicationContext();
        btnLogin = findViewById(R.id.btnLogin);
        edtEmail = findViewById(R.id.edtEmail);
        edtContraseña = findViewById(R.id.edtContraseña);
        mAuth = FirebaseAuth.getInstance();


        sqliteRoutines = new SqliteRoutines(this);
        vendedor = sqliteRoutines.readVendedor();

        /*
        if(app.isConnected(this)){
            if (vendedor != null){
                email = vendedor.getCorreoUsuario();
                contraseña = vendedor.getContrasenaUsuario();
                updateLogin();
                flag = false;
            }
        }


         */



        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                email = edtEmail.getText().toString();
                contraseña = edtContraseña.getText().toString();
                if (email.equals("") && contraseña.equals("")) {
                    Toast.makeText(MainActivity.this, "se debe ingresar un correo y una contraseña",
                            Toast.LENGTH_LONG).show();
                } else if (email.equals("")) {
                    Toast.makeText(MainActivity.this, "se debe ingresar un correo",
                            Toast.LENGTH_LONG).show();
                } else if (contraseña.equals("")) {
                    Toast.makeText(MainActivity.this, "se debe ingresar una contraseña",
                            Toast.LENGTH_LONG).show();
                } else {

                    mAuth.signInWithEmailAndPassword(email, contraseña).addOnCompleteListener(MainActivity.this, new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if (task.isSuccessful()) {
                                // Sign in success, update UI with the signed-in user's information
                                Log.d(TAG, "signInWithEmail:success");
                                FirebaseInstanceId.getInstance().getInstanceId()
                                        .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                                            @Override
                                            public void onComplete(@NonNull Task<InstanceIdResult> task) {
                                                if (!task.isSuccessful()) {
                                                    Log.v("Token", "getInstanceId failed", task.getException());
                                                    return;
                                                }

                                                // Get new Instance ID token
                                                token = task.getResult().getToken();
                                                Log.v("Token_update", token);
                                                   updateLogin();
                                            }
                                        });
                            } else {
                                // If sign in fails, display a message to the user.
                                Log.w(TAG,"createUserWithEmail:failure", task.getException());
                                String Mendaje = String.valueOf(task.getException());
                                if(Mendaje.equals("com.google.firebase.auth.FirebaseAuthInvalidCredentialsException: The password is invalid or the user does not have a password.")){
                                    Toast.makeText(MainActivity.this, R.string.errorContraseña,
                                            Toast.LENGTH_SHORT).show();
                                }else {
                                    Toast.makeText(MainActivity.this, R.string.errorEmail,
                                            Toast.LENGTH_SHORT).show();
                                }


                            }
                        }
                    });




                }
            }
        });
    }

    // Metodo que me permite generar el update del login
    private void updateLogin() {
        Endpoints endpoints = RetrofitClientInstance.getRetrofitInstance().create(Endpoints.class);
        JSONObject dataLogin = new JSONObject();
        try {
            dataLogin.put("correoUsuario", email);
            dataLogin.put("contrasenaUsuario", contraseña);
            dataLogin.put("tokenDispositivo", token);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        Call<String> call = endpoints.loginUpdate(dataLogin.toString());
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                try {
                    Log.v("response", String.valueOf(response));
                   JSONObject jsonObject = new JSONObject(response.body());
                    String res = jsonObject.getString("res");
                    JSONArray jsonArray = new JSONArray(res);

                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jsonObjects = jsonArray.getJSONObject(i);

                        correoUsuario = jsonObjects.getString("correoUsuario");
                        idUsuario = jsonObjects.getInt("idUsuario");
                        rolUsuario = jsonObjects.getString("rolUsuario");
                        nombreVendedor = jsonObjects.getString("nombreVendedor");
                        apellidoVendedor = jsonObjects.getString("apellidoVendedor");
                        telefonoVendedor = jsonObjects.getString("telefonoVendedor");
                        idZonaVendedor = jsonObjects.getString("idZonaVendedor");
                        contrasenaUsuario = jsonObjects.getString("contrasenaUsuario");
                        nombreZona = jsonObjects.getString("nombreZona");

                            guardarVerdedor();


                    }


                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                Toast.makeText(MainActivity.this,
                        R.string.internet,
                        Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void guardarVerdedor() {

        if (flag){
            vendedor= new Vendedor(idUsuario, correoUsuario, rolUsuario, nombreVendedor,apellidoVendedor,
                    telefonoVendedor, idZonaVendedor,contrasenaUsuario,nombreZona);
            sqliteRoutines.saveVendedor(vendedor);
        }
        session.flag("false");
        Intent intent = new Intent(MainActivity.this, Home.class);
        startActivity(intent);
        finish();
    }

}